#!/bin/bash

# minisat2.2 on the hyperplane reduction with old interface  on the raw instances

# To set in a normal envirnement
inst=$2                         # nick name
mypath='binary'
TMPDIR=$inst
TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
BH=$mypath/best_hyperplane_old_interface # set this to the executable of hyperplane reducer
INPUT=$1                  # raw instance name
METHOD=$3

shift 

#Run Hyperplane reduction
echo "c"
echo "c Starting hyperplane reduction"
echo "c"
echo "c $BH $INPUT -hyper-set=$METHOD -num-bits=10  2>> $TMP.err"
echo -n "2 " >> $TMP.time
start=$(date +%s%N)
$BH $INPUT -hyper-set=$METHOD -num-bits=10  2>> $TMP.err # 2 generate cnf.(i+1), don't think 10 will make any difference
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo "return code" $X
if [ $X == 30 ]; then       # there could be contradiction in bit setting after hyperplane reduction
    exit $X
fi

echo "c"
echo "c Starting minisat on hyperplane reduction for 5000 seconds"
echo "c"
    # Run minisat on hyperplane reduction for 10 minutes
    # date
echo "c $RS -cpu-lim=5000 $INPUT.1 $TMP.result "$@" > $TMP.null"
echo -n "6 " >> $TMP.time
start=$(date +%s%N)
$RS -cpu-lim=5000 $INPUT.1 $TMP.result "$@" > $TMP.null # 6
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo "cs Minisat returned $X"
    # date
if [ $X != 10 ]; then
	exit $X
fi
    
exit $X
