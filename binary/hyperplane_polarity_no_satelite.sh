#!/bin/bash

# just running plain minisat on the hyperplane reduction on the raw
# instances

INPUT=$1
inst=$2
METHOD=$3
NUMBITS=$4
POL=$5
TIMEOUT=100

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
BH=$mypath/best_hyperplane # set this to the executable of hyperplane reducer

shift 

#Run Hyperplane reduction
echo "c"
echo "c Starting hyperplane reduction for at most 100 seconds"
echo "c"
echo "$BH $INPUT.cnf 10 2>> $TMP.err"
echo -n "2 " >> $TMP.time
start=$(date +%s%N)
timeout $TIMEOUT $BH $INPUT -hyper-set=$METHOD -bit-select=$BITSELECT -num-bits=10 2>> $TMP.err 
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
echo "return code" $X

if [ $X == 124 ]; then		
	echo "c hypeprplane computation timed out"
	echo "c"
	echo "c Starting minisat on original reduction"
	echo "c" 
	echo -n "7 " >> $TMP.time
	start=$(date +%s%N)
	# $RS $INPUT 
	$RS  -cpu-lim=5000  $INPUT
	X=$?
	spent=$(($(date +%s%N)-${start}))
	echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
	exit $X
else
	echo "c"
	echo "c Starting minisat on hyperplane reduction for the remaining time"
	echo "c"
    # Run minisat on hyperplane reduction for 10 minutes
    # date
	echo "c $RS -cpu-lim=5000 $INPUT.best.10 $TMP.result > $TMP.null"
	echo -n "6 " >> $TMP.time
	start=$(date +%s%N)
	$RS -cpu-lim=5000 -rnd-pol=$POL -hyper-path=$INPUT.best.10 $INPUT
	X=$?
	spent=$(($(date +%s%N)-${start}))
	echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

	echo "cs Minisat returned $X"
	exit $X
fi

    
