#!/bin/bash

# hyperplane minisat using the new interface, and removing the second
# run of satellite. inject the unsat clause only when the hyperplane is found
# unsat

if [ "x$1" = "x" ]; then
    echo "USAGE: hyperplane_minisat.sh <input CNF>"
    exit 1
fi

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
SE=$mypath/SatELite_release     # set this to the executable of SatELite
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
BH=$mypath/best_hyperplane # set this to the executable of hyperplane reducer
INPUT=$1;                               # 
METHOD=$3

shift 
echo "c"
echo "c Starting SatElite Preprocessing $TMP"
echo "c"
echo "$SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim"

echo -n "1 " >> $TMP.time
start=$(date +%s%N)
$SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim # 1
X=$?                 # find the error code of the last executed command.
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo 'return code' $X
if [ $X == 0 ]; then
    #SatElite terminated correctly
    #Run Hyperplane reduction
    echo "c"
    echo "c Starting hyperplane reduction"
    echo "c"
    echo "$BH $TMP.cnf -hyper-set=$METHOD -num-bits=10  2>> $TMP.err"
    echo -n "2 " >> $TMP.time
    start=$(date +%s%N)
    # $BH $TMP.cnf 10 2>> $TMP.err # 2 generate cnf.(i+1)
    $BH $TMP.cnf -hyper-set=$METHOD -num-bits=10  2>> $TMP.err
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    echo "return code" $X

    echo "c"
    echo "c SE returned $X...Starting minisat on hyperplane reduction for 10 minutes"
    echo "c"
    # Run minisat on hyperplane reduction for 10 minutes
    echo "$RS -cpu-lim=600 -hyper-path=$TMP.cnf.best.10 $TMP.cnf $TMP.result "$@" > $TMP.null.hyper"
    echo -n "3 " >> $TMP.time
    start=$(date +%s%N)
    $RS -cpu-lim=600 -hyper-path=$TMP.cnf.best.10 $TMP.cnf $TMP.result "$@" > $TMP.null.hyper
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    echo "cs Minisat returned $X"

    if [ $X != 10 ]; then
	    echo "c"
	    echo "c Starting minisat on original reduction"
	    echo "c" 
        
        if [ $X == 20 ]; then
        	#Hyperplane reduction unsat run minisat on original with unsat
	        #clause added
            echo "$RS -unsat-path=$TMP.cnf.best.10 $TMP.cnf $TMP.result "$@" > $TMP.null"
            echo -n "40 " >> $TMP.time
            start=$(date +%s%N)
	        $RS -unsat-path=$TMP.cnf.best.10 $TMP.cnf $TMP.result "$@" > $TMP.null
	        X=$?
            spent=$(($(date +%s%N)-${start}))
            echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
        else
            echo "$RS $TMP.cnf $TMP.result "$@" > $TMP.null"
            echo -n "41 " >> $TMP.time
            start=$(date +%s%N)
	        $RS $TMP.cnf $TMP.result "$@" > $TMP.null
	        X=$?
            spent=$(($(date +%s%N)-${start}))
            echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
        fi
        

	    if [ $X == 20 ]; then
	        echo "s UNSATISFIABLE"
	        #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	        exit 20
	        #Don't call SatElite for model extension.
	    elif [ $X != 10 ]; then
	        #timeout/unknown, nothing to do, just clean up and exit.
	        #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	        exit $X
	    fi 
    fi

    #Hyperplane reduction is SATISFIABLE, extend model 
    echo "extends model"
    echo "$SE --verbosity=0 +ext $INPUT $TMP.result $TMP.vmap $TMP.elim "$@""
    echo -n "5 "  >> $TMP.time
    start=$(date +%s%N)
    $SE --verbosity=0 +ext $INPUT $TMP.result $TMP.vmap $TMP.elim "$@" 
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    #rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.hp $TMP.result $TMP.null
elif [ $X == 11 ]; then # to many clauses, first best_hyperplane and run on the reduction
    echo "c"
    echo "c Starting hyperplane reduction on original problem"
    echo "c"
    
    echo "$BH $INPUT -hyper-set=$METHOD -num-bits=10  2>> $TMP.err"
    echo -n "6 " >> $TMP.time
    start=$(date +%s%N)
    # $BH $TMP.cnf 10 2>> $TMP.err # 2 generate cnf.(i+1)
    $BH $INPUT -hyper-set=$METHOD -num-bits=10  2>> $TMP.err
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    echo "return code" $X
    
    echo "c"
    echo "c SE returned $X...Starting minisat on hyperplane reduction for 10 minutes"
    echo "c"
    echo "$RS -cpu-lim=600 -hyper-path=$INPUT.best.10 $INPUT $TMP.result "$@" > $TMP.null.hyper"
    echo -n "7 " >> $TMP.time
    start=$(date +%s%N)
    $RS -cpu-lim=600 -hyper-path=$INPUT.best.10 $INPUT $TMP.result "$@" > $TMP.null.hyper
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    echo "cs Minisat returned $X"
    
    # date
    if [ $X != 10 ]; then
	    #Hyperplane reduction unsat or indet, run minisat on original
	    echo "c"
	    echo "c Starting minisat on original instance"
	    echo "c" 
        
        if [ $X == 20 ]; then
            echo "$RS -unsat-path=$INPUT.best.10 $INPUT"
            echo -n "80 " >>$TMP.time
            start=$(date +%s%N)
            $RS -unsat-path=$INPUT.best.10 $INPUT
            X=$?
            spent=$(($(date +%s%N)-${start}))
            echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
        else
            echo "$RS $INPUT"
            echo -n "81 " >>$TMP.time
            start=$(date +%s%N)
            $RS $INPUT
            X=$?
            spent=$(($(date +%s%N)-${start}))
            echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
        fi
        
	    if [ $X == 20 ]; then
	        echo "s UNSATISFIABLE"
	        #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	        # exit `expr 20 + 14`
            exit 20
	        #Don't call SatElite for model extension.
	    elif [ $X != 10 ]; then
	        #timeout/unknown, nothing to do, just clean up and exit.
	        #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	        exit $X
	    fi
    else
       cat $TMP.null.hyper 
    fi
    
elif [ $X == 12 ]; then
    #SatElite prints out usage message
    #There is nothing to do here.
    X=0
fi
#rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.cnf.1
exit $X
