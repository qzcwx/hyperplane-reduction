#!/bin/bash

# hyperplane minisat using the new interface, and removing the second
# run of satellite as well as the second run of hyperplane reduction
# after HR is UNSAT, return to the original isntance without preprocessed

if [ "x$1" = "x" ]; then
    echo "USAGE: hyperplane_minisat.sh <input CNF>"
    exit 1
fi

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
SE=$mypath/SatELite_release     # set this to the executable of SatELite
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
BH=$mypath/best_hyperplane # set this to the executable of hyperplane reducer
INPUT=$1;                               # 
METHOD=$3

shift 
echo "c"
echo "c Starting SatElite Preprocessing $TMP"
echo "c"
# echo "$SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim"

echo -n "1 " >> $TMP.time
start=$(date +%s%N)
$SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim # 1
X=$?                 # find the error code of the last executed command.
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo 'c return code' $X
if [ $X == 0 ]; then
    #SatElite terminated correctly
    #Run Hyperplane reduction
    echo "c"
    echo "c Starting hyperplane reduction"
    echo "c"
    echo "c $BH $TMP.cnf 10 2>> $TMP.err"
    echo -n "2 " >> $TMP.time
    start=$(date +%s%N)
    # $BH $TMP.cnf 10 2>> $TMP.err # 2 generate cnf.(i+1)
    $BH $TMP.cnf -hyper-set=$METHOD -num-bits=10  2>> $TMP.err
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    echo "c return code" $X
    
    echo "c"
    echo "c SE returned $X...Starting minisat on hyperplane reduction for 10 minutes"
    echo "c"
    # Run minisat on hyperplane reduction for 10 minutes
    echo -n "3 " >> $TMP.time
    start=$(date +%s%N)
    $RS -cpu-lim=2400 -hyper-path=$TMP.cnf.best.10 $TMP.cnf $TMP.result "$@" > $TMP.null.hyper
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    echo "c Minisat returned $X"
    # date
    if [ $X != 10 ]; then
	    #Hyperplane reduction unsat or indet, run minisat on original
	    echo "c"
	    echo "c Starting minisat on original reduction"
	    echo "c" 
        echo -n "4 " >> $TMP.time
        start=$(date +%s%N)
        $RS $INPUT                  
        X=$?
        spent=$(($(date +%s%N)-${start}))
        echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
        
	    if [ $X == 20 ]; then
	        echo "s UNSATISFIABLE"
	    fi
        exit $X
    fi

    #Hyperplane reduction is SATISFIABLE, extend model 
    echo "c extends model"
    echo "c $SE --verbosity=0 +ext $INPUT $TMP.result $TMP.vmap $TMP.elim "$@""
    echo -n "5 "  >> $TMP.time
    start=$(date +%s%N)
    $SE --verbosity=0 +ext $INPUT $TMP.result $TMP.vmap $TMP.elim "$@" 
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    #rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.hp $TMP.result $TMP.null
elif [ $X == 11 ]; then # to many clauses, first best_hyperplane and run on the reduction
	#Hyperplane reduction unsat or indet, run minisat on original
	echo "c"
	echo "c Starting minisat on original instance"
	echo "c" 
    
    echo -n "8 " >>$TMP.time
    start=$(date +%s%N)
    $RS $INPUT                  
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
	if [ $X == 20 ]; then
	    echo "s UNSATISFIABLE"
	    #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	    # exit `expr 20 + 14`
        exit 20
	    #Don't call SatElite for model extension.
	elif [ $X != 10 ]; then
	    #timeout/unknown, nothing to do, just clean up and exit.
	    #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	    exit $X
	fi
elif [ $X == 12 ]; then
    #SatElite prints out usage message
    #There is nothing to do here.
    X=0
fi
#rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.cnf.1
exit $X
