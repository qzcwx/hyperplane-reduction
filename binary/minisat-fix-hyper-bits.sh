#!/bin/bash
# running the lastest version of minisat 2.2, fixing hyperplane variables

# echo $1
# echo $2

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
INPUT=$1.cnf
RSEED=$6
solIndex=$9

#Hyperplane reduction unsat or indet, run minisat on original
#rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.cnf.1
echo "c"
echo "c Starting minisat on original reduction"
echo "c" 
echo "$RS $INPUT -rnd-pol=$POL -hyper-path=/s/chopin/b/grad/chenwx/solHyper/$inst.$solIndex -rnd-seed=$RSEED"
echo -n "7 " >> $TMP.time
start=$(date +%s%N)
# $RS $INPUT 
$RS $INPUT -rnd-pol=$POL -hyper-path=/s/chopin/b/grad/chenwx/solHyper/$1.$solIndex -rnd-seed=$RSEED
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
exit $X
