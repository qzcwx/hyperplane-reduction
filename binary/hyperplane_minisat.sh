#!/bin/bash

# original hyperplane minisat

if [ "x$1" = "x" ]; then
    echo "USAGE: hyperplane_minisat.sh <input CNF>"
    exit 1
fi

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
SE=$mypath/SatELite_release     # set this to the executable of SatELite
RS=$mypath/minisat_static       # set this to the executable of minisat
BH=$mypath/best_hyperplane # set this to the executable of hyperplane reducer
INPUT=$1;                               # 

shift 
echo "c"
echo "c Starting SatElite Preprocessing $TMP"
echo "c"
echo "$SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim"

echo -n "1 " >> $TMP.time
start=$(date +%s%N)

$SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim # 1
X=$?                 # find the error code of the last executed command.

spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo 'return code' $X
if [ $X == 0 ]; then
    #SatElite terminated correctly
    #Run Hyperplane reduction
    echo "c"
    echo "c Starting hyperplane reduction"
    echo "c"
    echo "$BH $TMP.cnf 10 2>> $TMP.err"
    echo -n "2 " >> $TMP.time
    start=$(date +%s%N)
    $BH $TMP.cnf 10 2>> $TMP.err # 2 generate cnf.(i+1), don't think 10 will make any difference
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    echo "return code" $X
    if [ $X == 30 ]; then       # there could be contradiction in bit setting after hyperplane reduction
        # hyperplane reduction unsat or indet, run minisat on original
        #rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.cnf.1
        echo "c"
        echo "c Starting minisat on original reduction"
        echo "c"

        echo -n "3 " >> $TMP.time
        start=$(date +%s%N)
        $RS $TMP.cnf $TMP.result "$@" > $TMP.null # 3
        X=$?
        spent=$(($(date +%s%N)-${start}))
        echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

        if [ $X == 20 ]; then
            echo "s UNSATISFIABLE"
            #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
            exit 20
            #Don't call SatElite for model extension.
        elif [ $X != 10 ]; then
            #timeout/unknown, nothing to do, just clean up and exit.
            #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
            exit $X
        fi 
        echo -n "4 " >> $TMP.time
        start=$(date +%s%N)
        #SATISFIABLE, call SatElite for model extension
        $SE +ext $INPUT $TMP.result $TMP.vmap $TMP.elim  "$@" # 4
        #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
        X=$?
        spent=$(($(date +%s%N)-${start}))
        echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

        exit 10
    fi
    case  in
    esac
    
    echo "c Starting SE"
    echo "c"

    echo -n "5 " >> $TMP.time
    start=$(date +%s%N)
    $SE $TMP.cnf.1 $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim 2>> $TMP.err # 5
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

    echo "c"
    echo "c SE returned $X...Starting minisat on hyperplane reduction for 10 minutes"
    echo "c"
    # Run minisat on hyperplane reduction for 10 minutes
    # date
    echo "c $RS -cpu-lim=600 $TMP.pre.cnf $TMP.pre.result"
    echo -n "6 " >> $TMP.time
    start=$(date +%s%N)
    $RS -cpu-lim=600 $TMP.pre.cnf $TMP.pre.result "$@" > $TMP.null # 6
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    echo "cs Minisat returned $X"
    # date
    if [ $X != 10 ]; then
	    #Hyperplane reduction unsat or indet, run minisat on original
	    #rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.cnf.1
	    echo "c"
	    echo "c Starting minisat on original reduction"
	    echo "c" 
        echo -n "7 " >> $TMP.time
        start=$(date +%s%N)
	    $RS $TMP.cnf $TMP.result "$@" > $TMP.null # 7
	    X=$?
        spent=$(($(date +%s%N)-${start}))
        echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

	    if [ $X == 20 ]; then
	        echo "s UNSATISFIABLE"
	        #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	        exit 20
	        #Don't call SatElite for model extension.
	    elif [ $X != 10 ]; then
	        #timeout/unknown, nothing to do, just clean up and exit.
	        #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	        exit $X
	    fi 
        echo -n "8 " >> $TMP.time
        start=$(date +%s%N)
	    #SATISFIABLE, call SatElite for model extension
	    $SE +ext $INPUT $TMP.result $TMP.vmap $TMP.elim  "$@" # 8
	    #rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.null
	    X=$?
        spent=$(($(date +%s%N)-${start}))
        echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
	    exit 10
    fi
    #Hyperplane reduction is SATISFIABLE, extend model 
    echo "c $SE +ext $TMP.cnf.1 $TMP.pre.result $TMP.pre.vmap $TMP.pre.elim"
    echo -n "9 " >> $TMP.time
    start=$(date +%s%N)
    $SE +ext $TMP.cnf.1 $TMP.pre.result $TMP.pre.vmap $TMP.pre.elim "$@" > $TMP.hp # 9
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    echo -n "10 " >> $TMP.time
    start=$(date +%s%N)
    cut -c3- $TMP.hp | sed '1d' | sed "1iSAT" > $TMP.result # 10
    spent=$(($(date +%s%N)-${start}))
    echo  "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    echo "c $SE --verbosity=0 +ext $INPUT $TMP.result $TMP.vmap $TMP.elim"
    
    echo -n "11 "  >> $TMP.time
    start=$(date +%s%N)
    $SE --verbosity=0 +ext $INPUT $TMP.result $TMP.vmap $TMP.elim "$@" # 11
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
    
    #rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.hp $TMP.result $TMP.null
elif [ $X == 11 ]; then
    #SatElite died, glucose must take care of the rest
    echo "c"
    echo "c Starting minisat on original problem"
    echo "c"
    echo -n "12 " >>$TMP.time
    start=$(date +%s%N)
    $RS $INPUT                  # 12
    X=$?
    spent=$(($(date +%s%N)-${start}))
    echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
elif [ $X == 12 ]; then
    #SatElite prints out usage message
    #There is nothing to do here.
    X=0
fi
#rm -f $TMP.cnf $TMP.vmap $TMP.elim $TMP.result $TMP.cnf.1
exit $X
