#!/bin/bash

# just running plain minisat on the hyperplane reduction on the raw
# instances

if [ "x$1" = "x" ]; then
    echo "USAGE: hyperplane_minisat.sh <input CNF>"
    exit 1
fi

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
BH=$mypath/best_hyperplane  # set this to the executable of hyperplane reducer
INPUT=$1
METHOD=$3
shift 

#Run Hyperplane reduction
echo "c"
echo "c Starting hyperplane reduction"
echo "c"
echo "$BH $INPUT -hyper-set=$METHOD -num-bits=10 10 2>> $TMP.err"
echo -n "2 " >> $TMP.time
start=$(date +%s%N)
$BH $INPUT -hyper-set=$METHOD -num-bits=10 10 2>> $TMP.err # 2 generate cnf.(i+1), don't think 10 will make any difference
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo "return code" $X
if [ $X == 30 ]; then       # there could be contradiction in bit setting after hyperplane reduction
    exit $X
fi

echo "mv $INPUT.best.10 $TMP.best.10"
mv $INPUT.best.10 $TMP.best.10

echo "c"
echo "c Starting minisat on hyperplane reduction for 5000 seconds"
echo "c"
    # Run minisat on hyperplane reduction for 10 minutes
    # date
echo "$RS -cpu-lim=5000 $INPUT -hyper-path=$TMP.cnf.best.10 $TMP.result "$@" > $TMP.null"
echo -n "6 " >> $TMP.time
start=$(date +%s%N)
$RS -cpu-lim=5000 $INPUT -hyper-path=$TMP.best.10 $TMP.result "$@" > $TMP.null # 6
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo "cs Minisat returned $X"
    # date
if [ $X != 10 ]; then
	exit $X
fi
    
exit $X
