# running linging and save its time in file

inst=$2
mypath='binary'
solver=$mypath/'lingeling'
TMPDIR=$inst
INPUT=$1

TMP=$TMPDIR/$TMPDIR

echo $solver $INPUT 
echo -n "7 " >> $TMP.time
start=$(date +%s%N)
$solver $INPUT > $TMP.null # 7
# $solver $INPUT 
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

exit $X
