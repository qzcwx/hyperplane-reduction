#!/bin/bash

# running minisat_blbd and save its time in file

inst=$2
mypath='binary'
solver=$mypath/'blbd_pol'
TMPDIR=$inst
INPUT=$1
POL=$5
SEED=$6;

TMP=$TMPDIR/$TMPDIR

echo -n "7 " >> $TMP.time
echo "$solver $INPUT -rnd-pol=$POL -rnd-seed=$SEED -wal-order=$ORDER -pol-path=$TMP.pol -count-path=$TMP"
start=$(date +%s%N)
$solver $INPUT -cpu-lim=5000 -rnd-pol=$POL -rnd-seed=$SEED -pol-path=$TMP.pol -count-path=$TMP
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
exit $X
