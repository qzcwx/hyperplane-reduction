#!/bin/bash
# running the lastest version of minisat 2.2

# echo $1
# echo $2

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
INPUT=$1;                               # 
POL=$5;
shift

#Hyperplane reduction unsat or indet, run minisat on original
#rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.cnf.1
echo "c"
echo "c Starting minisat on original reduction"
echo "c" 
echo -n "7 " >> $TMP.time
start=$(date +%s%N)
# $RS $INPUT 
$RS $INPUT -no-pre -rnd-pol=$POL
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

exit $X
