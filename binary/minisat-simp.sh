#!/bin/bash

# echo $1
# echo $2

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
RS=$mypath/minisat2.2-hyper       # set this to the executable of minisat
INPUT=$1;                               # 
POL=$5;
SEED=$6;
ORDER=$7;
WALSH=$8;
shift

#Hyperplane reduction unsat or indet, run minisat on original
#rm -f $TMP.pre.cnf $TMP.pre.vmap $TMP.pre.elim $TMP.pre.result $TMP.cnf.1
echo "c"
echo "c Starting minisat on original reduction"
echo "c" 
echo -n "7 " >> $TMP.time
start=$(date +%s%N)
# $RS $INPUT 
if [ $WALSH != -1 ]; then
	$RS $INPUT -rnd-pol=$POL -rnd-seed=$SEED -wal-order=$ORDER -max-clause-walsh=$WALSH
else
	$RS $INPUT -rnd-pol=$POL -rnd-seed=$SEED -wal-order=$ORDER
fi
X=$?
spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time
exit $X
