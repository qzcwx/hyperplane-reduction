#!/bin/bash
# running satelite before minisat

inst=$2

# To set in a normal envirnement
mypath='binary'
TMPDIR=$inst

TMP=$TMPDIR/$inst        # set this to the location of temporary files, $$ is the process ID
SE=$mypath/SatELite_chen       # set this to the executable of minisat
INPUT=$1;                                
shift 
echo "c"
echo "c Starting SatElite Preprocessing $TMP"
echo "c"
echo "c $SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim"

echo -n "1 " >> $TMP.time
start=$(date +%s%N)

$SE $INPUT $TMP.cnf $TMP.vmap $TMP.elim # 1
X=$?                 # find the error code of the last executed command.

spent=$(($(date +%s%N)-${start}))
echo "$(echo "scale=9;$spent/(1*10^09)" | bc)" >> $TMP.time

echo 'c return code' $X
exit $X
