# runOne.sh
# take parameter as input, and runs only one stance, and run them on local disk

import subprocess as sp
import re
import os
import shutil
import time
import sys
import datetime
import socket

def flush(out, err, dirName):
    path = "{}/{}.cnf".format(dirName, dirName)

    # store stdout
    with open(path+".out", 'w') as f:
        f.write(out)

    # store stderr
    with open(path+".err", 'w') as f:
        f.write(err)


# solver = 'hyperplane_minisat.sh'
# solver = 'minisat-only.sh'
# solver = 'minisat-new.sh'
# solver = 'lingeling.sh'
# solver = 'hyperplane_only.sh'
# solver = 'hyperplane_reduction.sh'
# solver = 'hyperplane_reduction_no_satellite.sh'
# solver = 'hyperplane_reduction_minisat2.sh'
# solver = 'hyperplane_reduction_minisat2_old_interface.sh'
# solver = 'hyperplane_minisat_inject.sh'


limit = 5000

print 'argv', sys.argv
if len(sys.argv)==10:
    # os.system('./build.sh 2>/dev/null 1>/dev/null')
    nickname = sys.argv[1]
    path = sys.argv[2]
    method = sys.argv[3]
    solver = sys.argv[4]
    bitSelect = sys.argv[5]
    rndPol = sys.argv[6]
    seed = sys.argv[7]
    order = sys.argv[8]
    walsh = sys.argv[9]
    
    print 'method:', method, 'solver:', solver, 'bitSelect:', bitSelect, 'rndPol:', rndPol, 'seed:',  seed, 'order:', order, 'maxClauseWalsh', walsh
    
    match = re.search(r'(.+/)(.+)(.cnf)', path)
    solverName = re.search(r'(.+).sh',solver).group(1)
    dirName = "{}-{}-M{}-B{}-R{}-S{}-O{}-W{}".format(solverName, nickname, method, bitSelect, rndPol, seed, order, walsh)
    nickInstName = nickname+'.cnf'        # nick instance name with ".cnf"
    print str(datetime.datetime.now()), 'instance', dirName
    
    # create directory for dumping out
    if os.path.exists(dirName):
        shutil.rmtree(dirName)
    os.makedirs(dirName)

    # create an empty file for storing runtime
    open('{}/{}.time'.format(dirName, dirName), 'a').close()
    
    # copy inst.gz to current directory
    # shutil.copy(path+'.gz', nickInstName+'.gz')   # with gz
    shutil.copy(path, nickInstName) # without gz, plain text file
    
    # # decompress instance
    # start = time.time()
    # os.system("gunzip -f "+nickInstName+'.gz')
    # t = time.time() - start
    # with open('{}/{}.time'.format(dirName, dirName), 'a') as timefile:
    #     print >>timefile, '-1 {}'.format(t) # just the decompressing time, can be ignore

    start = time.time()
    # if method:                            # true for enabling rnd-pol
    #     p = sp.Popen(['timeout', str(limit), 'binary/'+solver, nickInstName, dirName, '-rnd-pol'], stdout=sp.PIPE, stderr=sp.PIPE)
    # else:                                 # false for disabling rnd-pol
    #     p = sp.Popen(['timeout', str(limit), 'binary/'+solver, nickInstName, dirName, '-no-rnd-pol'], stdout=sp.PIPE, stderr=sp.PIPE)
    # print ['timeout', str(limit), 'binary/'+solver, nickInstName, dirName, method]
    
    # p = sp.Popen(['timeout', str(limit), 'binary/'+solver, nickInstName, dirName, method, bitSelect, rndPol, seed, order, walsh], stdout=sp.PIPE, stderr=sp.PIPE)
    p = sp.Popen(['binary/'+solver, nickInstName, dirName, method, bitSelect, rndPol, seed, order, walsh], stdout=sp.PIPE, stderr=sp.PIPE)

    out, err = p.communicate()
    t = time.time() - start
    
    flush(out, err,  dirName)
    # print 'out', out
    # print 'err', err
    
    incomplete = False
    try:
        # check if runtime file is incomplete
        with open('{}/{}.time'.format(dirName, dirName), 'r') as timefile:
            s = timefile.read()
            if s[-1]!='\n':
                # print 'incomplete'
                # print s
                # print [float(i) for i in re.findall(" [\d.]+\n",s)]
                remain = sum([float(i) for i in re.findall(" [\d.]+\n",s)])
                incomplete = True
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

    # append overall time to *.time
    with open('{}/{}.time'.format(dirName, dirName), 'a') as timefile:
        if incomplete == True:
            print >>timefile, t - remain
        print >>timefile, '0 {}'.format(t)

    # 124 if COMMAND times out
    # 125 if `timeout' itself fails
    # 126 if COMMAND is found but cannot be invoked
    # 127 if COMMAND cannot be found
    # 137 if COMMAND is sent the KILL(9) signal (128+9)
    # the exit status of COMMAND otherwise
    
    print str(datetime.datetime.now()), 'return', p.returncode
    
    # save return code in file
    with open('{}/{}.ret'.format(dirName,dirName), 'w') as retfile:
        print >>retfile, p.returncode

    # compressing results and instance
    os.system("gzip -r "+dirName)

    os.remove(nickInstName)
    # uplaod result directory back to laptop
    # os.system('lftp desktop -e "mirror -R {}/{} /s/chopin/m/proj/sched/chenwx/new/{} ; exit"'.format(os.getcwd(), dirName, dirName))
    os.system('lftp laptop -e "mirror -R {}/{} /home/chenwx/workspace/result-hyperplane-reduction/new/{} ; exit"'.format(os.getcwd(), dirName, dirName))
    
    # remove the result directory to save space
    shutil.rmtree(dirName)
else:
    print 'USAGE: python runOne.py [nickname] [path to instance] [method id] [solver]'
