# python script for analysing html page of SAT competition
# also provide a variety of ranges tools

from bs4 import BeautifulSoup
import re
import numpy as np
import os

def storeTable():
    # store the table
    dat = dict()
    for i in range(3,len(rows)):
         row = rows[i]
         col = row.findAll('a')
         # print col[0]
         line = []
         ID = int(re.findall(r'instance=(\d+)', str(col[0]))[0])
         # build mapping betweeen ID to exact path
         inst[ID] = ( nickname[i-3], str(col[0].get_text()), inst[ID])

         for j in [len(col)-3,len(col)-2,len(col)-1]:
             # print float(re.findall("\d+.\d+",col[j].get_text())[0]),
             line.append(float(re.findall("\d+.\d+",col[j].get_text())[0]))
         # print
         dat[ID]=(line)

def findWorse():
    # find instances that our algorithm is worse
    worse = []
    for key,value in dat.items():
        if value[0] == 50000.0 and not (value[1]==50000.0 and value[2]==50000.0):
            # print key, value
            worse.append(key)

    # # print the full path of worse instances
    # prefix = '/home/chenwx/workspace/inst/'
    # for i in worse:
    #     print '%s%s' %(prefix,inst[i][1])

    # print worse instances as table for easy
    print '|inst|Doug|Zenn 0.1.0|Lingeling aqw|'
    for i in worse:
        print '|{}|{}|{}|{}|'.format(inst[i][0],int(dat[i][0]),int(dat[i][1]),int(dat[i][2]))

def allInstancePath():
    # # on laptop
    # prefix = '/home/chenwx/workspace/inst/sat13/'

    # # on desktop home
    # prefix = '/s/chopin/b/grad/chenwx/sat13/'

    # on desktop home
    prefix = '/s/chopin/b/grad/chenwx/sched/sat13/'

    for v in inst.values():
        print v[0], prefix+v[2]

def allInstancePathFilter():
    # print the path of all instances filtered by specified file

    prefix = '/s/chopin/b/grad/chenwx/sched/sat13/'
    # prefix = '/home/chenwx/workspace/inst/sat13/'

    # fname = '/home/chenwx/workspace/result-hyperplane-reduction/ret-code-11.instance'
    # fname = '/home/chenwx/workspace/result-hyperplane-reduction/minisat2.2-simp-less-than-60s'
    # fname = '/home/chenwx/workspace/result-hyperplane-reduction/diffHyper.instance'
    # fname = '/home/chenwx/workspace/result-hyperplane-reduction/hr-ret-code-20.instance'
    fname = '/home/chenwx/workspace/result-hyperplane-reduction/HR0-ret-20.instance'

    with open(fname, 'r') as f:
        string = f.read()
        for v in inst.values():
            if v[0] in string:
                print v[0], prefix+v[2]



def checkExist():
    # check if dirname exists
    prefix = '/home/chenwx/workspace/result-hyperplane-reduction/new/'
    # solver = 'hyperplane_reduction'
    # solver = 'hyperplane_only'
    # solver = 'hyperplane_reduction_minisat2'
    # solver = 'hyperplane_minisat_simp_No2HR'
    # solver = 'hyperplane_reduction_satelite_WCHR'
    solver = 'minisat-new'


    mRange = [2]
    # fname = '/home/chenwx/workspace/result-hyperplane-reduction/ret-code-not-11.instance'
    fname = '/home/chenwx/workspace/satcode/hyperplane_reduction/all.instance'

    with open(fname, 'r') as f:
        fstr = f.read()
        for v in fstr.split():
            for m in mRange:
                path = prefix+solver+'-'+v+'-'+str(m)
                # print path
                if not os.path.exists(path):
                    # print v+'-'+str(m), 'not exist'
                    print v+'-'+str(m)

        # for v in inst.values():
        #     for m in mRange:
        #         # print v[0]
        #         if not os.path.exists(prefix+solver+'-'+v[0]+'-'+str(m)):
        #             print v[0]+'-'+str(m), 'not exist'


if __name__ == '__main__':
    # analyze the html file
    soup = BeautifulSoup(open("result.html"))
    alltable=soup.findAll('table')
    table=alltable[2]
    rows = table.findAll('tr')

    inst = dict()

    # retrieve list of filenames
    table1 = alltable[1]
    path = table1.findAll('option')
    for p in path:
        # print p
        inst[int(p['value'])] = str(p.get_text())

    nickname = ['001a','001b','001c','001d','002a','002b','002c','003a','003b','003c','003d','003e', '004a', '004b', '005a', '005b', '005c', '006a', '006b', '007a', '007b', '007c', '007d', '008a', '008b', '008c', '009a', '009b', '009c', '010', '9vliw-bug1', '9vliw-bug10', '9vliw-bug4', '9vliw-bug6', '9vliw-bug7', '9vliw-bug8', '9vliw-bug9', 'aaai10-pathways-17', 'aaai10-ipc5', 'ACG-15-10p1', 'ACG-20-5p1', 'aes-16', 'aes-24-2', 'aes-24-4', 'aes-32-1', 'aes-32-2', 'aes-64', 'AProVE07-11', 'AProVE09-06', 'b04-s-2', 'b04-s', 'b04-s-pre', 'blocks-130', 'blocks-150', 'E02F20', 'E02F22', 'E04F19', 'esawn-uw3', 'grid-3.045', 'grid-3.055', 'grid-3.065', 'grieu-vmpc-31', 'gss-17-s100', 'gss-18-s100', 'gss-19-s100', 'gss-20-s100', 'gss-21-s100', 'gss-22-s100', 'gss-23-s100', 'gss-24-s100', 'gss-25-s100', 'hitag2-7', 'hitag2-8', 'itox-vc1033', 'itox-vc1130', 'md5-47-1', 'md5-47-2', 'md5-47-3', 'md5-47-4', 'md5-48-1', 'md5-48-2', 'md5-48-3', 'md5-48-4', 'md5-48-5', 'ndhf-xits-19', 'ndhf-xits-21', 'openstacks', 'partial-10-11-s', 'partial-10-17-s', 'partial-10-19-s', 'partial-5-13-s', 'partial-5-15-s', 'partial-5-17-s', 'partial-5-19-s', 'pb-200-03-lb-03', 'pb-300-01-lb-00', 'pb-300-02-lb-06', 'pb-300-02-lb-07', 'pb-300-03-lb-13', 'pb-300-05-lb-16', 'pb-300-05-lb-17', 'pb-300-06-lb-02', 'pb-300-09-lb-07', 'pb-300-10-lb-12', 'pb-300-10-lb-13', 'pb-400-02-lb-15', 'pb-400-04-lb-19', 'pb-400-05-lb-00', 'pb-400-09-lb-05', 'pb-400-10-lb-00', 'post-cbmc', 'q-query-3', 'rbcl-xits-14', 'rpoc-xits-15', 'slp-top25', 'slp-top26', 'slp-top27', 'smtlib-aigs', 'total-10-15-s', 'transport-city-25-020', 'transport-city-25-030', 'transport-city-25-040', 'transport-city-25-050', 'transport-city-25-060', 'transport-city-35-020', 'transport-city-35-030', 'transport-city-35-040', 'transport-three-020', 'transport-three-030', 'transport-two-020', 'transport-two-030', 'transport-two-040', 'UCG-15-10p1', 'UR-15-10p1', 'UR-20-10p1', 'UR-20-5p1', 'UTI-15-10p1', 'UTI-20-10p1', 'UTI-20-5p1', 'velev-npe-1.0-9dlx-b71', 'velev-pipe-sat-1.0-b7', 'velev-pipe-sat-1.0-b9', 'vmpc-29', 'vmpc-30', 'vmpc-32', 'vmpc-33', 'vmpc-34', 'vmpc-35', 'vmpc-36', 'zfcp-2.8-u2-nh']

    storeTable()
    # findWorse()
    # checkExist()
    allInstancePathFilter()
