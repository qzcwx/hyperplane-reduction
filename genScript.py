#!/usr/bin/python

# script for generating a list of commands. saved in commands.lst
import re, os

resDir = '/home/chenwx/workspace/result-hyperplane-reduction/new'

# outfile = 'commands.lst.laptop'
# instFile = 'hard.laptop'

# outfile = 'commands.lst.laptop'
# instFile = '2013.2014.indu.sat.laptop'

outfile = 'commands.lst'
instFile = '2014.indu.sat.desk'

# instFile = 'itox.inst'
# instFile = 'all'
# instFile = 'all.neg'

# outfile = 'commands.lst.laptop.neg'
# outfile = 'commands.lst.neg'

# instFile = 'hyperplane.no'
# instFile = 'ret-code-11'
# instFile = 'diffHyper'
# instFile = 'hr-ret-code-20'
# instFile = 'allLaptop.neg'
# instFile = 'HR0-ret-20'

prefix = 'python runOneLocal.py'
# methodRange = [0,1,2,3]
methodRange = [0]
# methodRange = [2]
# methodRange = [0,1]
# solverRange = ['hyperplane_minisat_inject.sh', 'hyperplane_minisat_simp.sh']
# solverRange = ['hyperplane_reduction_satelite_inject_WCHR.sh']
# solverRange = ['hyperplane_reduction_satelite_WCHR.sh']
# solverRange = ['minisat-only.sh']
# solverRange = ['hyperplane_reduction_minisat2.sh']
# solverRange = ['satelite.sh']
# solverRange = ['hyperplane_polarity_no_satelite.sh']
# solverRange = ['hyperplane_reduction_no_satellite.sh']
# solverRange = ['blbd_pol.sh']
# solverRange = ['minisat_blbd.sh']
# solverRange = ['minisat-hyper-act.sh']
solverRange = ['minisat_blbd_act.sh']

bitSelectRange = [1]
rndPolRange = [5]
seedRange = [1]
# seedRange = range(1,11)
orderRange = [1]
maxClauseWalsh = [-1]
# orderRange = [1, 1, 2, 6]
# maxClauseWalsh = [-1, -1, -1, 6]

with open(instFile, 'r') as f, open(outfile, 'w') as out:
    for inst in f:
        sepInst = inst.split()
        if not sepInst: continue                      # skip empty lines
        for bitSelect in bitSelectRange:
            for method in methodRange:
                for rndPol in rndPolRange:
                    for solver in solverRange:
                        for seed in seedRange:
                            for i in range(len(orderRange)):
                                order = orderRange[i]
                                walsh = maxClauseWalsh[i]
                                # rndPol = rndPolRange[i]
                                # print 'order', order, 'walsh', walsh
                                # for walsh in maxClauseWalsh:
                                nickname = sepInst[0] # nickname of instance
                                solverName = re.search(r'(.+).sh',solver).group(1)
                                resDirname = "{}/{}-{}-M{}-B{}-R{}-S{}-O{}-W{}".format(resDir, solverName, nickname, method, bitSelect, rndPol, seed, order, walsh)
                                # print resDirname
                                if not os.path.exists(resDirname):
                                    dirName = nickname+'-'+str(method)
                                    path = sepInst[1]
                                    # directory = re.search('(.+/)sat13',path).group(1)
                                    print >>out, 'cd ~/sched/; cp -r hyperplane_reduction /tmp/{}; cd /tmp/{}; {} {} {} {} {} {} {} {} {} {}; rm -rf /tmp/{}; cd ~/sched/hyperplane_reduction;'.format(dirName, dirName, prefix, nickname, path, method, solver, bitSelect, rndPol, seed, order, walsh, dirName)
                                else:
                                    print resDirname, 'exist'
