#!/usr/bin/python

# script for generating a list of commands. saved in commands.lst
import re, os

def lookupInstPath(inst):
    # mapfile = '/home/chenwx/workspace/satcode/hyperplane_reduction/allLaptop'
    mapfile = '/home/chenwx/workspace/satcode/hyperplane_reduction/hard.desk'
    with open(mapfile, 'r') as f:
        for line in f:
            terms = line.split()
            if terms[0]==inst:
               return terms[1]


resDir = '/home/chenwx/workspace/result-hyperplane-reduction/new'
# outfile = 'commands.lst.laptop'
# instFile = 'hard.laptop'

# instFile = 'allLaptop'
# instFile = '/home/chenwx/workspace/result-hyperplane-reduction/2014.only.sat'
outfile = 'commands.lst'

# instFile = 'all'
# instFile = 'all.neg'

# outfile = 'commands.lst.laptop.neg'
# outfile = 'commands.lst.neg'

# instFile = 'hyperplane.no'
# instFile = 'ret-code-11'
# instFile = 'diffHyper'
# instFile = 'hr-ret-code-20'

# instFile = 'allLaptop.neg'
# instFile = 'HR0-ret-20'

prefix = 'python runOneLocalFixBits.py'
# methodRange = [0,1,2,3]
methodRange = [0]
# methodRange = [2]
# methodRange = [0,1]
# solverRange = ['hyperplane_minisat_inject.sh', 'hyperplane_minisat_simp.sh']
# solverRange = ['hyperplane_reduction_satelite_inject_WCHR.sh']
# solverRange = ['hyperplane_reduction_satelite_WCHR.sh']
solverRange = ['minisat-fix-hyper-bits.sh']
# solverRange = ['minisat-only.sh']
# solverRange = ['hyperplane_reduction_minisat2.sh']
# solverRange = ['satelite.sh']
# solverRange = ['hyperplane_polarity_no_satelite.sh']
# solverRange = ['hyperplane_reduction_no_satellite.sh']
# solverRange = ['blbd_pol.sh']

bitSelectRange = [1]
rndPolRange = [0]
seedRange = [1]
orderRange = [0]
maxClauseWalsh = [-1]
# orderRange = [1,2, 6]
# maxClauseWalsh = [-1, -1, 6]

files = os.listdir('/home/chenwx/workspace/result-hyperplane-reduction/aaai/solHyper')

with open(outfile, 'w') as out:
    for f in files:
        res = re.search('(.+)\.(.+)', f)
        nickname = res.group(1) # nickname of instance
        solIndex = res.group(2)
        print nickname, solIndex
        path = lookupInstPath(nickname)
        for bitSelect in bitSelectRange:
            for method in methodRange:
                for rndPol in rndPolRange:
                    for solver in solverRange:
                        for seed in seedRange:
                            for order in orderRange:
                                walsh = maxClauseWalsh[orderRange.index(order)]
                                solverName = re.search(r'(.+).sh',solver).group(1)
                                resDirname = "{}/{}-{}-M{}-B{}-R{}-S{}-O{}-W{}-I{}".format(resDir, solverName, nickname, method, bitSelect, rndPol, seed, order, walsh, solIndex)
                                if not os.path.exists(resDirname):
                                    dirName = nickname+'-'+str(method)
                                    # directory = re.search('(.+/)sat13',path).group(1)
                                    print >>out, 'cd ~/sched/; cp -r hyperplane_reduction /tmp/{}; cd /tmp/{}; {} {} {} {} {} {} {} {} {} {} {}; rm -rf /tmp/{}; cd ~/sched/hyperplane_reduction;'.format(dirName, dirName, prefix, nickname, path, method, solver, bitSelect, rndPol, seed, order, walsh, solIndex, dirName)
                                else:
                                    print resDirname, 'exist'
