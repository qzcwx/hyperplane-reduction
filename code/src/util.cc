/* my implementation of common utilities for c */
#include "util.h"
#include <stdio.h>
#include <stdlib.h>

/* initialze list, reture 1 if successful, otherwise return 0 */
list* list_init(Type type)
{
  list* l = (list*) malloc(sizeof(list));
  if (l)
    {
      l->len = 0;
      l->cap = LIST_CAP;
      l->item_list = malloc(list_sizeof(type) * l->cap);
      l->type = type;
      /* printf ("cap = %d\n",l->cap); */
    }
  return l;
}

list* list_add_item(list* l, void* data, Type type)
{

  /* check space in list for the new data item  */
  /* printf ("add item\n"); */
  if (l->len >= l->cap)
    {
      /* printf ("realloc, cap = %d \n", l->cap * 2); */
      l->item_list = realloc(l->item_list, list_sizeof(type) * l->cap * 2);
      l->cap = l->cap * 2;
    }
  /* printf ("len = %d, cap = %d\n", l->len, l->cap); */
  switch(type)
    {
    case INT:
      /* printf ("INT=%d\n",INT); */
      /* ((int*)l->item_list)[l->len] = *((int*)data); */
      list_get(l, l->len, int) = *((int*)data);
      l->len++;
      break;
    default:
      printf ("in add_item: wrong type %d\n",type); 
      exit(-1);
    }
  return l;
}

int list_sizeof(Type type)
{
  /* printf ("sizeof\n"); */
  /* printf ("INT = %d\n",INT); */
  switch(type)
    {
    case INT:
      return sizeof(int);
      break;
    default:
      printf ("in sizeof: wrong type %d\n",type); 
      exit(-1);      
    }  
}

/*  list_get(list*l, int i,Type type) */
/* { */
/*   if (i < l->len) */
/*     { */
/*       switch(type): */
/*         { */
/*         case INT: */
/*           return ((int*) l->item_list)[i]; */
/*           break; */
/*         default: */
/*           printf ("in sizeof: wrong type %d\n",type);  */
/*           exit(-1);       */
/*         } */
/*     } */
/*   else */
/*     { */
/*       printf ("index out of bound\n"); */
/*       exit(-1); */
/*     } */
/* } */


/* remove all emlement in list l */
void list_clear(list* l)
{                               /* simply set len to 0 */
  /* free(l->item_list); */
  l->len = 0;
}


void list_free(list* l)
{
  free(l->item_list);
  free(l);
}


/* void main() */
/* { */
/*   int i, len=10; */
  
/*   list* l = list_init(INT); */
/*   for (i=0; i<len; i++) */
/*     { */
/*       l = list_add_item(l, &i, INT); */
/*     } */
/*   for (i=0; i<l->len; i++) */
/*     { */
/*       /\* printf ("%d\t", ((int*) l->item_list)[i]); *\/ */
/*       printf ("%d\t", list_get(l, i, int) ); */

/*     } */
/*   printf ("\n"); */

/*   list_clear(l); */
/*   printf ("after clear\n"); */
/*   for (i=0; i<l->len; i++) */
/*     { */
/*       /\* printf ("%d\t", ((int*) l->item_list)[i]); *\/ */
/*       printf ("%d\t", list_get(l, i, int) ); */
/*     } */
/*   printf ("\n"); */
  
/*   list_free(l); */
/* } */

