#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "var.h"
#include "varclause.h"
#include "varwalsh.h"
#include "util.h"

int int_cmp_pure (const void *a, const void *b)
{
  const int *ia = (const int *)a; // casting pointer types 
  const int *ib = (const int *)b;
  return *ib  - *ia;
}

/* check if that variable asscoated with any Walsh term */
/* look for ID of Walsh term */
/* vars <- bits */
int check_varwcof(var *sol, varw_vec *w_prime, int *vars, int order) {
  int i, j, found;
  varw_cof *check;
  
  // printf("start check_varwcof\n");
  // printf("sol[vars[0]].num_w_cofs = %d\n", sol[vars[0]].num_w_cofs);
  if(sol[vars[0]].num_w_cofs == 0) { /* only need to use the first variable for looking up  related Walsh term */
    return -1;
  } else {
    for(i = 0; i < sol[vars[0]].num_w_cofs; i++) {
	  // printf("i=%d\n",i);
      // check = &(w_prime->wb[sol[vars[0]].w_cofs[i]]);
      check = &(*w_prime)[sol[vars[0]].w_cofs[i]];
	  // printf("check = %d\n",check);
      found = 1;
      if(check->order == order) {
        for(j = 0; j < check->order; j++) {
          if(vars[j] != check->vars[j]) {
            found = 0;
          }
        }
      } else {
        found = 0;
      }
      if(found) {
        return check->id;
      }
    }
	// printf("end loop\n");
  }
  // printf("end check_varwcof\n");
  return -1;
}

// int reduce_instance(varclause* instance, varclause **newinstance, var *sol, int *vari, int *value, int m, int NUMBITS) {
//   int i,j, k;
//   int new_m = 0;
//   int idx, nv, bit, bitval, clause_true;
//   int vars[7000], signs[7000];
//   int *unsat;
  
//   /* printf ("BEGIN reduce_instance\n"); */
//   unsat = (int*)malloc(sizeof(int) * m); /* for storing unsat clauses */
  
//   for(i = 0; i < m; i++) {
//     unsat[i] = 0;               /* assuming all UNSAT with start with */
//   }
  
//   /* Find SAT clauses */
//   for(i = 0; i < NUMBITS; i++) { /* propogate bits fixed by hyperplane one by one  */
//     /* associate sol to vars, which is clearly WRONG! */
//     bit = vari[i];
//     bitval = value[i];
//     /* printf ("i = %d\tbit=%d\tbitval= %d\n", i, bit, bitval); */
//     for(j = 0; j < sol[i].num_clauses; j++) { 
//       /* propogate to all clauses associated with (i.idx)th variable of 'sol', 
//          how to avoid revalute the same clause multiple times, watch out for 'flag' 
         
//          OPTIMIZATION: Doug doesn't seem to use the flag to avoid
//          checking the assume clause multiple times
//       */
//       varclause *vc = sol[i].clauses[j];
      
//       clause_true = 0; /* assume it can not be satisfied to start with */
//       for(k = 0; k < vc->numvars; k++) { 
//         /* printf ("k=%d, vc->var[k]=%d\n", k, vc->vars[k]); */
//         if(vc->vars[k] == bit) {
//           if((bitval && (vc->signs[k] > 0)) || (!bitval && (vc->signs[k] < 0))) { /* as long as one variable is satisfied */
//             clause_true = 1;
//             /* printf ("clause true\n"); */
//             break;
//           }
//         }
//       }
//       if(clause_true) {
//         unsat[vc->id] = 1;      /* vc->id clause is already SAT */
//       } 		
//     }
//   }
  
//   /* printf ("unsat\n"); */
//   /* for ( i = 0; i < m; ++i) */
//   /*   { */
//   /*     printf ("%d",unsat[i]); */
//   /*   } */
//   /* printf ("\n"); */

//   /* Count number of unsat clauses */
//   new_m = 0;
//   for(i = 0; i < m; i++) {
//     if(!unsat[i]) {             /* if 0 (UNSAT) */
//       new_m++;
//     }
//   }

//   /* printf ("new_m = %d\n", new_m); */
  
//   /* allocate new instance
//      the additional clauses?
//      we now know new_m clause are UNSAT under current bit setting fixed by hyperplane
//      adding the clauses to pruning the search space
//   */
//   (*newinstance) = (varclause*) malloc(sizeof(varclause) * new_m);
  
//   /* generate new instance */
//   new_m = 0; /* new_m is reset to 0 ????? that should be OK, since the space is already allocated */
//   idx = 0;
//   for(i = 0; i < m; i++) {      /* go through all clauses */
//     if(!unsat[i]) {             /* if clause is UNSAT */
//       int fixed_bits = 0;
//       nv = 0;                   /* number of 'free' variables  */
//       for(j = 0; j < instance[i].numvars; j++) { /* look for variables fixed by hyperplane in an UNSAT clause */
//         int bit_flag = 1;
//         for(k = 0; k < NUMBITS; k++) {
//           if(instance[i].vars[j] == vari[k]) { 
//             bit_flag = 0; /* current clause contains a variable fixed by hyperplane */
//           }
//         }
//         if(bit_flag) {	        /* not contain any fixed variable */
//           vars[nv] = instance[i].vars[j]; /* where is 'instance' coming from? */
//           signs[nv] = instance[i].signs[j];
//           nv++;
//         } else {                /* contains a fixed variables */
//           fixed_bits++;
//         }
//       }
//       if(fixed_bits == instance[i].numvars) { /* if all bits in the UNSAT clause is fixed, then there is no way to satisfy */
//         /* printf ("i =  %d\n",i); */
//         printf("c c Contradiction in bit setting\n");
//         exit(30);
//       }
//       if(nv > 0) { /* if current UNSAT clause contains at least one free
//                       variable, the fixed variables must be used to
//                       satisfy the clause */
//         // printf("c init_varclause_nowf");
//         init_varclause_nowf(&((*newinstance)[idx]), nv, vars, signs); /* initialize Walsh for the new, which maybe unnessary for just hyperplane reduction */
//         (*newinstance)[idx].id = idx;
//         idx++;
//         new_m++;
//       } 		
//     }
//   }

//   free(unsat);
//   /* printf ("END reduce_instance\n"); */
//   return new_m;                /* return number of additional clauses */
// }


int varwalsh_sign(var *sol, varw_cof *w) {
  int bc = 0;
  int i;

  for(i = 0; i < w->order; i++) {
    if(w->vars[i] > -1) {
      if(sol[w->vars[i]].value > 0) {
        bc++;
      }
    }
  }

  if(bc % 2 == 0) {
    return 1;
  } else {
    return -1; 
  }
}

int varwalsh_sign_int(int *sol, varw_cof *w) {
  int bc = 0;
  int i;

  for(i = 0; i < w->order; i++) {
    if(w->vars[i] > -1) {
      if(sol[w->vars[i]] > 0) {
        bc++;
      }
    }
  }

  if(bc % 2 == 0) {
    return 1;
  } else {
    return -1; 
  }
}

void compute_varwalsh_signs(var *sol, varw_vec *w_prime) {
  unsigned i;

  // for(i = 0; i < w_prime->num_w_cofs; i++) {
  for(i = 0; i < (*w_prime).size(); i++) {
    // w_prime->wb[i].sign = varwalsh_sign(sol, &w_prime->wb[i]);
    (*w_prime)[i].sign = varwalsh_sign(sol, &(*w_prime)[i]);
  }	
}

/* PARAMETER:
   NUMBITS -> max 
   sol: a solution
   
   RETURN:
   best_vars 
*/
void compute_varhyperplanes(varw_vec *w_prime, int n, int max, var *sol, var *reduced_sol, int *best_vars) {
  /* max <- NUMBITS */
  int i, j, k;
  int nv, idx;
  int *vars;
  double hp_fitness;
  int *vars_fake;
  int *signs_fake;
  int order;
  int *bits;
  int w_id;
  int *pos;
  
  /* double *zeros; */
  /* double *ones;  */
  varclause *fakeinstance; /* fakeinstance relates to the assignemnt to hyperplane */
  fakeinstance = (varclause*) malloc(sizeof(varclause) * 1); /* fakeinstance is just a single clause */
  vars = (int*) malloc(sizeof(int) * n);               /* full size vector, which I think 'max' size is already sufficient */
  vars_fake = (int*) malloc(sizeof(int) * max);        
  signs_fake = (int*) malloc(sizeof(int) * max);
  pos = (int*) malloc(sizeof(int)* max);
  
  idx = 0;                      /* idx for fakeinstance, which is 1 */
  i=0;
  nv=0;
  
  /* printf ("BEGIN compute_varhyperplanes\n"); */
  /* printf ("sol.idx\n"); */
  /* for (j=0; j<max; j++) */
  /*   { */
  /*     printf ("%d\n",sol[j].idx); */
  /*   } */
  
  for(j = 0; j < max; j++) {    /* pick the top 'max' variables */
    vars_fake[nv] = sol[j].idx; /* why 4 first? */
    signs_fake[nv] = 1;         /* all true to start with... */
    pos[j] = j;
    nv++;                       /* what's the purpose of using nv */
  }

  /* printf ("var_fake\n"); */
  /* for (j = 0; j < max; ++j) */
  /*   { */
  /*     printf ("%d\n",vars_fake[j]); */
  /*   } */

  /* printf ("nv = %d, max = %d\n", nv, max); */
  /* sort top 'max' variables in natural order, and save them in fakeinstance[0] */
  init_varclause_pos(&fakeinstance[idx], nv, vars_fake, signs_fake, pos); 
  /* printf ("fakeinstance vars\n"); */
  /* for (j = 0; j < fakeinstance[idx].numvars; j++) */
  /*   { */
  /*     printf ("%d\n", fakeinstance[idx].vars[j]); */
  /*   } */
  
  /* printf ("pos\n"); */
  /* for (j=0; j<max; j++) */
  /*   { */
  /*     printf ("%d",pos[j]); */
  /*   } */
  /* printf ("\n"); */
  
  fakeinstance[idx].id = idx;   /* clause id */
  /* printf ("numvars = %d\n",fakeinstance[idx].numvars); */
  bits = (int*) malloc(sizeof(int) * fakeinstance[idx].numvars);
  for(j = 1; j < pow(2,fakeinstance[idx].numvars); j++) { /* for each candidate hyperplanes, why j instead of (j+1)? Because the orignal is all 1s, we only need to move (2^NUMBITS - 1) steps */
    /* count the order of hyperplane candidate */
    /* printf ("\nj = %d\t",j); */
    order = 0;
    for(k = 0; k < fakeinstance[idx].numvars; k++) {
      /* also appears compute_walsh */
      if(CHECK_BIT(j,k)) {      
        /* printf ("1"); */
        bits[order] = fakeinstance[idx].vars[k];
        order++;
      }
      else
        {
          /* printf ("0"); */
        }
    }
    /* printf ("\n"); */
    /* printf ("order = %d\n",order); */

    for(k = order; k < fakeinstance[idx].numvars; k++) {
      bits[k] = -1;
    }

    /* printf ("bits\n"); */
    /* for(k = 0; k < fakeinstance[idx].numvars; k++) { */
    /*   printf ("%d\t",bits[k]); */
    /* } */
    /* printf ("\n"); */

    /* reduced_sol for checking involving Walsh coefficients */
    w_id = check_varwcof(reduced_sol, w_prime, bits, order); 
    /* printf ("w_id = %d\n",w_id); */
    
    if(w_id != -1) {            /* walsh transform for the fake instance */
      // fakeinstance[idx].w_cofs[fakeinstance[idx].cur_wcof++] = &(w_prime->wb[w_id]);
      fakeinstance[idx].w_cofs[fakeinstance[idx].cur_wcof++] = &((*w_prime)[w_id]);
    }
  }
  free(bits);
  idx++;
  
  double best_hp = -DBL_MAX;    /* initialize it to the smallest
                                   double-type variable */
  i=0;
  if(fakeinstance[i].numvars == max) { /* it has to be, if fakeinstance contains only one clause */
    for(j = 0; j < pow(2,fakeinstance[i].numvars); j++) { /* checking all 2^NUMBITS candidate hyperplane  */
      hp_fitness = 0;
      
      /* Set bits for hyperplane */
      for(k = 0; k < fakeinstance[i].numvars; k++) { /* for high index to low */
        if(CHECK_BIT(j,k)) {
          vars[fakeinstance[i].vars[k]] = 1; /* set bits fixed by hyperplane  */
        } else {
          vars[fakeinstance[i].vars[k]] = 0;
        }
      }

      printf ("\nvar\t");    /*  var with fakeinstance's index ordering */
      for (k=0; k<max; k++)
        {
          printf ("%d\t", fakeinstance[i].vars[k]);
        }
      printf ("\n");
      printf ("value\t");
      for (k=0; k<max; k++)
        {
          printf ("%d\t", vars[fakeinstance[i].vars[k]]);
        }
      printf ("\n");
      printf ("hyperplane\n");
      for (k=0; k<max; k++)
        {
          if (vars[fakeinstance[i].vars[k]]==1)
            {
              printf ("%d\t",(fakeinstance[i].vars[k])+1);
            }
          else if (vars[fakeinstance[i].vars[k]]==0)
            {
              printf ("%d\t",-(fakeinstance[i].vars[k]+1));
            }
        }
      printf ("0\t");
      /* printf ("\n"); */

      /* Compute average hyperplane fitness */
      for(k = 0; k < fakeinstance[i].cur_wcof; k++) {
        hp_fitness += varwalsh_sign_int(vars, fakeinstance[i].w_cofs[k]) * (fakeinstance[i].w_cofs[k])->value;
      }
      printf ("%f\n", hp_fitness); /* hp_fit without w_0 */
      
      if(hp_fitness > best_hp) {
        /* printf ("best update\n"); */
        best_hp = hp_fitness;
        for( k=0; k<max; k++) {
          best_vars[k] = vars[fakeinstance[i].vars[k]]; /* value for variables fixed by the best hyperplane */
        }
      }
      
      /* Record values in hyperplane */
      for(k = 0; k < fakeinstance[i].numvars; k++) {
        fakeinstance[i].hp[j].value[k] = vars[fakeinstance[i].vars[k]]; /* its related value */
        fakeinstance[i].hp[j].vars[k] = fakeinstance[i].vars[k]; /* index */
      }
      fakeinstance[i].hp[j].fitness = hp_fitness; 
      
      // if(hp_fitness > 0) {
      //   fakeinstance[i].total_hp_fitness += hp_fitness;
      // }
    }
  }

  /* printf ("\nbest_hp = %f\n",best_hp); */
  /* printf ("best_var\n"); */
  /* for (k=0; k<max; k++) */
  /*   { */
  /*     printf ("%d",best_vars[k]); */
  /*   } */
  /* printf ("\n"); */
  
  /*
    if(w_prime->hyperplane_bias[i] <= 0.01) {
    w_prime->hyperplane_bias[i] = 0.01;
    }
    if(w_prime->hyperplane_bias[i] >= 0.99) {
    w_prime->hyperplane_bias[i] = 0.99;
    }
  */
  

  /* /\* matching sol[i].idx using swap *\/ */
  int* copy;
  copy = (int*) malloc(sizeof(int)*max);
  
  /* first make a copy of best_vars */
  for (j = 0; j < max; j++)
    {
      copy[j] = best_vars[j];
    }
  for (j = 0; j < max; j++)
    {
      best_vars[pos[j]] = copy[j];
    }

  free(copy);
  free(pos);
  free(vars);

  
  /* printf ("END compute_varhyperplanes\n"); */
}

void compute_varhyperplanes_export(varw_vec *w_prime, int n, int max, var *sol, var *reduced_sol, int *best_vars, char* instance_name) {
  /* export hyperplane and its averges to file */

  /* max <- NUMBITS */
  int i, j, k;
  int nv, idx;
  int *vars;
  double hp_fitness;
  int *vars_fake;
  int *signs_fake;
  int order;
  int *bits;
  int w_id;
  int *pos;
  /* for all hyperplanes and its average */
  char outfile[10000];          
  FILE *outfp;
  /* best hyperplane */
  char bestfile[10000];
  FILE *bestfp;
  
  /* double *zeros; */
  /* double *ones;  */
  varclause *fakeinstance; /* fakeinstance relates to the assignemnt to hyperplane */
  fakeinstance = (varclause*) malloc(sizeof(varclause) * 1); /* fakeinstance is just a single clause */
  vars = (int*) malloc(sizeof(int) * n);               /* full size vector, which I think 'max' size is already sufficient */
  vars_fake = (int*) malloc(sizeof(int) * max);        
  signs_fake = (int*) malloc(sizeof(int) * max);
  pos = (int*) malloc(sizeof(int)* max);
  
  idx = 0;                      /* idx for fakeinstance, which is 1 */
  i=0;
  nv=0;
  
  /* printf ("BEGIN compute_varhyperplanes\n"); */
  /* printf ("sol.idx\n"); */
  /* for (j=0; j<max; j++) */
  /*   { */
  /*     printf ("%d\n",sol[j].idx); */
  /*   } */
  
  for(j = 0; j < max; j++) {    /* pick the top 'max' variables */
    vars_fake[nv] = sol[j].idx; /* why 4 first? */
    signs_fake[nv] = 1;         /* all true to start with... */
    pos[j] = j;
    nv++;                       /* what's the purpose of using nv */
  }

  /* printf ("var_fake\n"); */
  /* for (j = 0; j < max; ++j) */
  /*   { */
  /*     printf ("%d\n",vars_fake[j]); */
  /*   } */

  /* printf ("nv = %d, max = %d\n", nv, max); */
  /* sort top 'max' variables in natural order, and save them in fakeinstance[0] */
  init_varclause_pos(&fakeinstance[idx], nv, vars_fake, signs_fake, pos); 
  /* printf ("fakeinstance vars\n"); */
  /* for (j = 0; j < fakeinstance[idx].numvars; j++) */
  /*   { */
  /*     printf ("%d\n", fakeinstance[idx].vars[j]); */
  /*   } */
  
  /* printf ("pos\n"); */
  /* for (j=0; j<max; j++) */
  /*   { */
  /*     printf ("%d",pos[j]); */
  /*   } */
  /* printf ("\n"); */
  
  fakeinstance[idx].id = idx;   /* clause id */
  /* printf ("numvars = %d\n",fakeinstance[idx].numvars); */
  bits = (int*) malloc(sizeof(int) * fakeinstance[idx].numvars);
  for(j = 1; j < pow(2,fakeinstance[idx].numvars); j++) { /* for each candidate hyperplanes, why j instead of (j+1)? Because the orignal is all 1s, we only need to move (2^NUMBITS - 1) steps */
    /* count the order of hyperplane candidate */
    /* printf ("\nj = %d\t",j); */
    order = 0;
    for(k = 0; k < fakeinstance[idx].numvars; k++) {
      /* also appears compute_walsh */
      if(CHECK_BIT(j,k)) {      
        /* printf ("1"); */
        bits[order] = fakeinstance[idx].vars[k];
        order++;
      }
      else
        {
          /* printf ("0"); */
        }
    }
    /* printf ("\n"); */
    /* printf ("order = %d\n",order); */

    for(k = order; k < fakeinstance[idx].numvars; k++) {
      bits[k] = -1;
    }

    /* printf ("bits\n"); */
    /* for(k = 0; k < fakeinstance[idx].numvars; k++) { */
    /*   printf ("%d\t",bits[k]); */
    /* } */
    /* printf ("\n"); */

    /* reduced_sol for checking involving Walsh coefficients */
    w_id = check_varwcof(reduced_sol, w_prime, bits, order); 
    /* printf ("w_id = %d\n",w_id); */
    
    if(w_id != -1) {            /* walsh transform for the fake instance */
      fakeinstance[idx].w_cofs[fakeinstance[idx].cur_wcof++] = &((*w_prime)[w_id]);
    }
  }
  free(bits);
  idx++;
  
  double best_hp = -DBL_MAX;    /* initialize it to the smallest
                                   double-type variable */
  sprintf(outfile,"%s.hyper.%d", instance_name, max);
  outfp = fopen(outfile, "w");
  sprintf(bestfile, "%s.best.%d", instance_name, max);
  bestfp = fopen(bestfile, "w");
  

  i=0;
  if(fakeinstance[i].numvars == max) { /* it has to be, if fakeinstance contains only one clause */
    for(j = 0; j < pow(2,fakeinstance[i].numvars); j++) { /* checking all 2^NUMBITS candidate hyperplane  */
      hp_fitness = 0;
      
      /* Set bits for hyperplane */
      for(k = 0; k < fakeinstance[i].numvars; k++) { /* for high index to low */
        if(CHECK_BIT(j,k)) {
          vars[fakeinstance[i].vars[k]] = 1; /* set bits fixed by hyperplane  */
        } else {
          vars[fakeinstance[i].vars[k]] = 0;
        }
      }

      /* printf ("\nvar\t");    /\*  var with fakeinstance's index ordering *\/ */
      /* for (k=0; k<max; k++) */
      /*   { */
      /*     printf ("%d\t", fakeinstance[i].vars[k]); */
      /*   } */
      /* printf ("\n"); */
      /* printf ("value\t"); */
      /* for (k=0; k<max; k++) */
      /*   { */
      /*     printf ("%d\t", vars[fakeinstance[i].vars[k]]); */
      /*   } */
      /* printf ("\n"); */

      /* printf ("hyperplane\n"); */
      for (k=0; k<max; k++)
        {
          if (vars[fakeinstance[i].vars[k]]==1)
            {
              /* printf ("%d\t",(fakeinstance[i].vars[k])+1); */
              fprintf (outfp,"%d\t",(fakeinstance[i].vars[k])+1);
            }
          else if (vars[fakeinstance[i].vars[k]]==0)
            {
              /* printf ("%d\t",-(fakeinstance[i].vars[k]+1)); */
              fprintf (outfp, "%d\t", -(fakeinstance[i].vars[k]+1));
            }
        }
      /* printf ("0\t"); */
      fprintf (outfp,"0\t");
      /* printf ("\n"); */

      /* Compute average hyperplane fitness */
      for(k = 0; k < fakeinstance[i].cur_wcof; k++) {
        hp_fitness += varwalsh_sign_int(vars, fakeinstance[i].w_cofs[k]) * (fakeinstance[i].w_cofs[k])->value;
      }
      /* printf ("%f\n", hp_fitness); /\* hp_fit without w_0 *\/ */
      fprintf (outfp,"%f\n", hp_fitness); /* hp_fit without w_0 */
      
      if(hp_fitness > best_hp) {
        /* printf ("best update\n"); */
        best_hp = hp_fitness;
        for( k=0; k<max; k++) {
          best_vars[k] = vars[fakeinstance[i].vars[k]]; /* value for variables fixed by the best hyperplane */
        }
      }
      
      /* Record values in hyperplane */
      for(k = 0; k < fakeinstance[i].numvars; k++) {
        fakeinstance[i].hp[j].value[k] = vars[fakeinstance[i].vars[k]]; /* its related value */
        fakeinstance[i].hp[j].vars[k] = fakeinstance[i].vars[k]; /* index */
      }
      fakeinstance[i].hp[j].fitness = hp_fitness; 
      
      // if(hp_fitness > 0) {
      //   fakeinstance[i].total_hp_fitness += hp_fitness;
      // }
    }
  }

  /* printf ("\nbest_hp = %f\n",best_hp); */
  /* printf ("best_var\n"); */
  /* for (k=0; k<max; k++) */
  /*   { */
  /*     printf ("%d",best_vars[k]); */
  /*   } */
  /* printf ("\n"); */
  
  /*
    if(w_prime->hyperplane_bias[i] <= 0.01) {
    w_prime->hyperplane_bias[i] = 0.01;
    }
    if(w_prime->hyperplane_bias[i] >= 0.99) {
    w_prime->hyperplane_bias[i] = 0.99;
    }
  */
  

  /* /\* matching sol[i].idx using swap *\/ */
  int* copy;
  copy = (int*) malloc(sizeof(int)*max);
  
  /* first make a copy of best_vars */
  for (j = 0; j < max; j++)
    {
      copy[j] = best_vars[j];
    }
  for (j = 0; j < max; j++)
    {
      best_vars[pos[j]] = copy[j];
    }
  
  /* save best_vars in file */
  for (j = 0; j < max; j++)
    {
      if (best_vars[j] == 1)
        {
          fprintf(bestfp, "%d\t", (sol[j].idx+1));
        }
      else
        {
          fprintf(bestfp, "%d\t", -(sol[j].idx+1));
        }
    }

  fclose(bestfp);
  fclose(outfp);
  free(copy);
  free(pos);
  free(vars);
  /* printf ("END compute_varhyperplanes\n"); */
}


void compute_varhyperplanes_export_random(varw_vec *w_prime, int n, int max, var *sol, var *reduced_sol, int *best_vars, char* instance_name) { 
  /*  export hyperplane and its averges to file
      break tie randomly, rather than always selecting the first best move
  */
  /* max <- NUMBITS */
  int i, j, k;
  int nv, idx;
  int *vars;
  double hp_fitness;
  int *vars_fake;
  int *signs_fake;
  int order;
  int *bits;
  int w_id;
  int *pos;
  /* for all hyperplanes and its average */
  char outfile[10000];          
  FILE *outfp;
  /* best hyperplane */
  // char bestfile[10000];
  // FILE *bestfp;
  
  list* l=NULL;            /* list for keeping all equally best moves */
  int best_j=-1;           /* an intial value can cause problem */
  /* int num_hp = -1; */
  
  varclause *fakeinstance; /* fakeinstance relates to the assignemnt to hyperplane */
  fakeinstance = (varclause*) malloc(sizeof(varclause) * 1); /* fakeinstance is just a single clause */
  vars = (int*) malloc(sizeof(int) * n);               /* full size vector, which I think 'max' size is already sufficient */
  vars_fake = (int*) malloc(sizeof(int) * max);        
  signs_fake = (int*) malloc(sizeof(int) * max);
  pos = (int*) malloc(sizeof(int)* max);
  
  idx = 0;                      /* idx for fakeinstance, which is 1 */
  i=0;
  nv=0;
  
#ifdef NDEBUG
  printf ("BEGIN compute_varhyperplanes\n");
  printf ("sol.idx\n");
  for (j=0; j<max; j++)
    {
      printf ("%d\n",sol[j].idx+1);
    }
#endif  

  for(j = 0; j < max; j++) {    /* pick the top 'max' variables */
    vars_fake[nv] = sol[j].idx; /* why 4 first? */
    signs_fake[nv] = 1;         /* all true to start with... */
    pos[j] = j;
    nv++;                       /* what's the purpose of using nv */
  }

#ifdef NDEBUG
  printf ("var_fake\n");
  for (j = 0; j < max; ++j)
    {
      printf ("%d\n",vars_fake[j]+1);
    }
#endif

  /* printf ("nv = %d, max = %d\n", nv, max); */
  /* sort top 'max' variables in natural order, and save them in fakeinstance[0] */
  init_varclause_pos(&fakeinstance[idx], nv, vars_fake, signs_fake, pos); 
  /* printf ("fakeinstance vars\n"); */
  /* for (j = 0; j < fakeinstance[idx].numvars; j++) */
  /*   { */
  /*     printf ("%d\n", fakeinstance[idx].vars[j]); */
  /*   } */
  
  /* printf ("pos\n"); */
  /* for (j=0; j<max; j++) */
  /*   { */
  /*     printf ("%d",pos[j]); */
  /*   } */
  /* printf ("\n"); */
  
  fakeinstance[idx].id = idx;   /* clause id */
  /* printf ("numvars = %d\n",fakeinstance[idx].numvars); */
  bits = (int*) malloc(sizeof(int) * fakeinstance[idx].numvars);

  for(j = 1; j <  pow(2,fakeinstance[idx].numvars); j++) { /* for each candidate hyperplanes, why j instead of (j+1)? Because the orignal is all 1s, we only need to move (2^NUMBITS - 1) steps */
    /* count the order of hyperplane candidate */
    /* printf ("\nj = %d\t",j); */
    order = 0;
    for(k = 0; k < fakeinstance[idx].numvars; k++) {
      /* also appears in compute_walsh */
      if(CHECK_BIT(j,k)) {      
        /* printf ("1"); */
        bits[order] = fakeinstance[idx].vars[k];
        order++;
      }
      else
        {
          /* printf ("0"); */
        }
    }
    /* printf ("\n"); */
    /* printf ("order = %d\n",order); */

    for(k = order; k < fakeinstance[idx].numvars; k++) {
      bits[k] = -1;
    }

    // printf ("bits\n");
    // for(k = 0; k < fakeinstance[idx].numvars; k++) {
    //   printf ("%d\t",bits[k]);
    // }
    // printf ("\n");
	
    /* reduced_sol for checking involving Walsh coefficients */
    w_id = check_varwcof(reduced_sol, w_prime, bits, order); 
    // printf ("w_id = %d\n",w_id);
	
    if(w_id != -1) {            /* walsh transform for the fake instance */
      fakeinstance[idx].w_cofs[fakeinstance[idx].cur_wcof++] = &((*w_prime)[w_id]);
    }
  }
  free(bits);
  idx++;
  // printf("free bits\n");
  double best_hp = -DBL_MAX;    /* initialize it to the smallest
                                   double-type variable */
  sprintf(outfile,"%s.hyper.%d", instance_name, max);
  outfp = fopen(outfile, "w");
  // sprintf(bestfile, "%s.best.%d", instance_name, max);
  // bestfp = fopen(bestfile, "w");
  // printf("open outfile\n");
  
  
  i=0;
  l = list_init(INT);
  
  if(fakeinstance[i].numvars == max) { /* it has to be, if fakeinstance contains only one clause */
    for(j = 0; j < pow(2,fakeinstance[i].numvars); j++) { /* checking all 2^NUMBITS candidate hyperplane  */
      hp_fitness = 0;

      // printf ("j = %d\n",j);
      /* Set bits for hyperplane */
      for(k = 0; k < fakeinstance[i].numvars; k++) { /* for high index to low */
        if(CHECK_BIT(j,k)) {
          vars[fakeinstance[i].vars[k]] = 1; /* set bits fixed by hyperplane  */
        } else {
          vars[fakeinstance[i].vars[k]] = 0;
        }
      }
      
      /* printf ("\nvar\t");    /\*  var with fakeinstance's index ordering *\/ */
      /* for (k=0; k<max; k++) */
      /*   { */
      /*     printf ("%d\t", fakeinstance[i].vars[k]); */
      /*   } */
      /* printf ("\n"); */
      /* printf ("value\t"); */
      /* for (k=0; k<max; k++) */
      /*   { */
      /*     printf ("%d\t", vars[fakeinstance[i].vars[k]]); */
      /*   } */
      /* printf ("\n"); */

      // printf ("hyperplane\n");
      for (k=0; k<max; k++)
        {
          if (vars[fakeinstance[i].vars[k]]==1)
            {
              /* printf ("%d\t",(fakeinstance[i].vars[k])+1); */
              fprintf (outfp,"%d\t",(fakeinstance[i].vars[k])+1);
            }
          else if (vars[fakeinstance[i].vars[k]]==0)
            {
              /* printf ("%d\t",-(fakeinstance[i].vars[k]+1)); */
              fprintf (outfp, "%d\t", -(fakeinstance[i].vars[k]+1));
            }
        }
      /* printf ("0\t"); */
      fprintf (outfp,"0\t");
      /* printf ("\n"); */
      
      /* Compute average hyperplane fitness */
      for(k = 0; k < fakeinstance[i].cur_wcof; k++) {
        hp_fitness += varwalsh_sign_int(vars, fakeinstance[i].w_cofs[k]) * (fakeinstance[i].w_cofs[k])->value;
      }
      /* printf ("%f\n", hp_fitness); /\* hp_fit without w_0 *\/ */
      fprintf (outfp,"%f\n", hp_fitness); /* hp_fit without w_0 */
      
      if(hp_fitness > best_hp) 
        {
        /* printf ("best update\n"); */
        /* an absolute better hp */
        best_hp = hp_fitness;
        
        list_clear(l);
        list_add_item(l, &j, INT);
        
        /* /\* not so soon for recording best_vars *\/ */
        /* for( k=0; k<max; k++) { */
        /*   best_vars[k] = vars[fakeinstance[i].vars[k]]; /\* value for variables fixed by the best hyperplane *\/ */
        /* } */

        }
      else if (hp_fitness == best_hp)
        {                       /* equally good add to l  */
          /* printf("c equal good\n"); */
          list_add_item(l, &j, INT);
        }
      
      
      /* Record values in hyperplane */
      for(k = 0; k < fakeinstance[i].numvars; k++) {
        fakeinstance[i].hp[j].value[k] = vars[fakeinstance[i].vars[k]]; /* its related value */
        fakeinstance[i].hp[j].vars[k] = fakeinstance[i].vars[k]; /* index */
      }
      fakeinstance[i].hp[j].fitness = hp_fitness; 
      
      // if(hp_fitness > 0) {
      //   fakeinstance[i].total_hp_fitness += hp_fitness;
      // }


    }
    best_j = list_get(l, rand()%(l->len), int);
    /* printf ("best_j = %d, len = %d\n",best_j, l->len); */
    /* save in best_vars */
    for( k=0; k<max; k++) {
      if(CHECK_BIT(best_j,k)) {
        best_vars[k] = 1; /* set bits fixed by hyperplane  */
      } else {
        best_vars[k] = 0;
      }
      /* best_vars[k] = vars[fakeinstance[i].vars[k]]; /\* save value for variables fixed by the best hyperplane *\/ */
    }
  }

  // printf ("\nbest_hp = %f\n",best_hp);
  /* printf ("best_var\n"); */
  /* for (k=0; k<max; k++) */
  /*   { */
  /*     printf ("%d",best_vars[k]); */
  /*   } */
  /* printf ("\n"); */
  /* printf ("best_j = %d, len = %d\n",best_j, l->len); */
  
  /*
    if(w_prime->hyperplane_bias[i] <= 0.01) {
    w_prime->hyperplane_bias[i] = 0.01;
    }
    if(w_prime->hyperplane_bias[i] >= 0.99) {
    w_prime->hyperplane_bias[i] = 0.99;
    }
  */
  

  /* /\* matching sol[i].idx using swap *\/ */
  int* copy;
  copy = (int*) malloc(sizeof(int)*max);
  
  /* first make a copy of best_vars */
  for (j = 0; j < max; j++)
    {
      copy[j] = best_vars[j];
    }
  for (j = 0; j < max; j++)
    {
      best_vars[pos[j]] = copy[j];
    }
  
  // /* save best_vars in file */
  // for (j = 0; j < max; j++)
  //   {
  //     if (best_vars[j] == 1)
  //       {
  //         fprintf(bestfp, "%d\t", (sol[j].idx+1));
  //       }
  //     else
  //       {
  //         fprintf(bestfp, "%d\t", -(sol[j].idx+1));
  //       }
  //   }

  // fclose(bestfp);
  fclose(outfp);
  free(copy);
  free(pos);
  free(vars);
  free(vars_fake);
  free(signs_fake);
  list_free(l);
  free(fakeinstance->w_cofs);
  free(fakeinstance->vars);
  free(fakeinstance->signs);
  for(i = 0; i < fakeinstance->numhp; i++) {
    free(fakeinstance->hp[i].vars);
    free(fakeinstance->hp[i].value);
  }
  free(fakeinstance->hp);
  free(fakeinstance);

  #ifdef NDEBUG
    printf ("END compute_varhyperplanes\n");
  #endif
}


int is_sat(var *sol, varclause *instance, int i) {
  int j;
  for(j = 0; j < instance[i].numvars; j++) {
    if((sol[instance[i].vars[j]].value && (instance[i].signs[j] > 0)) ||
       (!sol[instance[i].vars[j]].value && (instance[i].signs[j] < 0))) {
      return 1;
    }
  }
  return 0;
}


void init_varclause_nowf(varclausenow *c, int numvars, int *vars, int *signs) {
  /* initialize a clause without associating hyperplane */
  int i,j, tmp;
  c->numvars = numvars;
  // c->cur_wcof=0;
  // c->total_hp_fitness=0;
  c->vars = (int*) malloc(sizeof(int) * numvars);
  c->signs = (int*) malloc(sizeof(int) * numvars);
  // c->numsat = 0;
  /* sorting variable indices in descending order ... */
  for(i = 0; i < (numvars - 1); i++) {
    for(j = 0; j < (numvars - i - 1); j++) {
      if(vars[j] < vars[j+1]) {
        tmp = vars[j];
        vars[j] = vars[j+1];
        vars[j+1] = tmp;
        tmp = signs[j];
        signs[j] = signs[j+1];
        signs[j+1] = tmp;
      }
    }
  }

  for(i = 0; i < numvars; i++) {
    c->vars[i] = vars[i];
    c->signs[i] = signs[i];
  }
} 

// should have delay the space allocation util the top hyperplane variables are determined
void init_varclause(varclause *c, int numvars, int *vars, int *signs) {
  /* initialize a regular clause */
  int i,j, tmp;
  c->numvars = numvars;
  // c->w_cofs = (varw_cof**) malloc(sizeof(varw_cof*) * pow(2,numvars)); /* need to calculate walsh terms */
  c->w_cofs=NULL;
  c->cur_wcof=0;
  // c->total_hp_fitness=0;
  c->vars = (int*) malloc(sizeof(int) * numvars);
  c->signs = (int*) malloc(sizeof(int) * numvars);
  // c->numsat = 0;

  /* printf ("init_varclause: var\n"); */
  /* for ( i = 0; i < numvars; ++i) */
  /*   { */
  /*     printf ("%d",vars[i]); */
  /*   } */
  /* printf ("\n"); */

  /* sort variables in descending order */
  for(i = 0; i < (numvars - 1); i++) {
    for(j = 0; j < (numvars - i - 1); j++) {
      if(vars[j] < vars[j+1]) {
        tmp = vars[j];
        vars[j] = vars[j+1];
        vars[j+1] = tmp;
        tmp = signs[j];
        signs[j] = signs[j+1];
        signs[j+1] = tmp;
      }
    }
  }

  /* printf ("init_varclause after sort: var\n"); */
  /* for ( i = 0; i < numvars; ++i) */
  /*   { */
  /*     printf ("%d",vars[i]); */
  /*   } */
  /* printf ("\n"); */
  
  for(i = 0; i < numvars; i++) {
    c->vars[i] = vars[i];
    c->signs[i] = signs[i];
  }
  
  // c->numhp = pow(2,numvars);    /* number of hyperplanes for this clause */
  // c->hp = (varhyperplane*) malloc(sizeof(varhyperplane) * c->numhp); 
  // for(i = 0; i < c->numhp; i++) {
  //   c->hp[i].vars = (int*) malloc(sizeof(int) * numvars);
  //   c->hp[i].value = (int*) malloc(sizeof(int) * numvars);
  //   c->hp[i].fitness = 0;
  // }
}

void init_varclause_pos(varclause *c, int numvars, int *vars, int *signs, int* pos) {
  /* initialize a regular clause with pos for keeping original order */
  // printf("c init_varclause_pos, numvar=%d\n",numvars);
  int i,j, tmp;
  c->numvars = numvars;
  c->w_cofs = (varw_cof**) malloc(sizeof(varw_cof*) * pow(2,numvars)); /* need to calculate walsh terms */
  c->cur_wcof=0;
  // c->total_hp_fitness=0;
  c->vars = (int*) malloc(sizeof(int) * numvars);
  c->signs = (int*) malloc(sizeof(int) * numvars);
  // c->numsat = 0;
  
  /* printf ("init_varclause: var\n"); */
  /* for ( i = 0; i < numvars; ++i) */
  /*   { */
  /*     printf ("%d",vars[i]); */
  /*   } */
  /* printf ("\n"); */

  /* sort variables in descending order */
  for(i = 0; i < (numvars - 1); i++) {
    for(j = 0; j < (numvars - i - 1); j++) {
      if(vars[j] < vars[j+1]) {
        tmp = vars[j];
        vars[j] = vars[j+1];
        vars[j+1] = tmp;

        tmp = signs[j];
        signs[j] = signs[j+1];
        signs[j+1] = tmp;

        tmp = pos[j];
        pos[j] = pos[j+1];
        pos[j+1] = tmp;
      }
    }
  }

  /* printf ("init_varclause after sort: var\n"); */
  /* for ( i = 0; i < numvars; ++i) */
  /*   { */
  /*     printf ("%d",vars[i]); */
  /*   } */
  /* printf ("\n"); */
  
  for(i = 0; i < numvars; i++) {
    c->vars[i] = vars[i];
    c->signs[i] = signs[i];
  }
  
  c->numhp = pow(2,numvars);    /* number of hyperplanes for this clause */
  c->hp = (varhyperplane*) malloc(sizeof(varhyperplane) * c->numhp); 
  for(i = 0; i < c->numhp; i++) {
    c->hp[i].vars = (int*) malloc(sizeof(int) * numvars);
    c->hp[i].value = (int*) malloc(sizeof(int) * numvars);
    c->hp[i].fitness = 0;
  }
} 


void free_varclause(varclause *instance, int m) {
  int i,j;
  varclause *c;
  for(j = 0; j < m; j++) {
    c = &instance[j];
    for(i = 0; i < c->numhp;i++) {
      free(c->hp[i].vars);
      free(c->hp[i].value);
    }
    free(c->hp);
    free(c->vars);
    free(c->signs);
  }
  free(instance);
}

double sum_varwcof(varw_cof *w, varclause *instance, int *vars, int order) {
  int bc = 0;
  int i, j;

  for(i = 0; i < order; i++) {
    if(vars[i] > -1) {
      j = 0;
      while(instance->vars[j] != vars[i]) {
        j++;
        if(j >= instance->numvars) {
          fprintf(stderr,"Out of bounds error in sum_w_cof\n");
          exit(-1);
        }
      }
      if(instance->signs[j] < 0) {
        bc++;
      }
    }
  }

  if(bc % 2 == 0) {
    w->value -= 1/(pow(2,instance->orig_numvars)); 
  } else {
    w->value += 1/(pow(2,instance->orig_numvars)); 
  }

  instance->w_cofs[instance->cur_wcof++] = w;
  return w->value;
}

// initialize the latest pushed element in the vector
int new_varwcof(varw_vec *w_prime, varclause *instance, var *sol, int *vars, int order) {
  int i;
  varw_cof w_cof_tmp;
  (*w_prime).push_back(w_cof_tmp);
  
  ((*w_prime).back()).value = 0;
  (*w_prime).back().id = (*w_prime).size() - 1;
  (*w_prime).back().num_clause = 0;
  (*w_prime).back().clause = NULL;
  (*w_prime).back().order = order;
  (*w_prime).back().vars = (int*) malloc(sizeof(int) * order);
  
  for(i = 0; i < order; i++) {
    (*w_prime).back().vars[i] = vars[i];
    if(vars[i] > -1) {
      if(add_w_cof(sol, vars[i], (*w_prime).size()-1) == -1) {
        fprintf(stderr,"Failed adding w_cof %ld to variable %d\n", (*w_prime).size() - 1, vars[i]);
        return -1;
      }
    }
  }
  sum_varwcof(&(*w_prime).back(), instance, vars, order);

  return 0;
}


double process_varw(varw_vec *w_prime, var *sol, varclause *instance, int *vars, int order) {
  /* compute walsh coefficient for term with 'vars' pattern of order 'order' */
  /* instance: a clause of instance */
  /* vars <- bits */
  int w_id;
  /* printf ("w_id = %d\n", check_varwcof(sol, w_prime, vars, order)); */
  if((w_id = check_varwcof(sol, w_prime, vars, order)) == -1) { /* not Walsh terms associated */
    if(new_varwcof(w_prime, instance, sol, vars, order) == -1) { /* create and compute the Walsh term if not exists before */
      fprintf(stderr, "Failed adding new w_cof\n");
      return -1;
    } else {
      return 1;
    }
  } else {
    return abs(sum_varwcof(&(*w_prime)[w_id], instance, vars,order));
  }
}

void compute_walsh(varw_vec *w_prime, Var *sol, varclause *instance, int m) {
  int i,j,k;
  int order;
  int *bits;
  /* printf ("BEGIN compute_walsh\n"); */
  /* printf ("m = %d\n", m); */
  for(i = 0; i < m; i++) {      /* for each clause in the instance */
    /* printf ("i = %d\tnumvars = %d\n", i, instance[i].numvars); */
    bits = (int*) malloc(sizeof(int) * instance[i].numvars);
    /* printf ("instance[i].vars\n"); */
    /* for(j = 0; j < instance[i].numvars; j++) */
    /*   { */
    /*     printf ("%d\t",instance[i].vars[j]); */
    /*   } */
    /* printf ("\n"); */
    for(j = 1; j < pow(2,instance[i].numvars); j++) { /* for each variable in the clause */
      /* also start from 1 instead of zero
         ignore the constant Walsh term (the average over the entire space)? */ 
      /* printf ("j = %d\n", j); */
      order = 0;
      for(k = 0; k < instance[i].numvars; k++) {
        if(CHECK_BIT(j,k)) {    /* only when the related bit is '1', the variable is stored */
          bits[order] = instance[i].vars[k];
          order++;
          /* printf ("1"); */
        }
      }
      for(k = order; k < instance[i].numvars; k++) {
        bits[k] = -1;
        /* printf ("0"); */
      }
      /* printf ("\nbits\n"); */
      /* for(k = 0; k < instance[i].numvars; k++) */
      /*   { */
      /*     printf ("%d\t",bits[k]); */
      /*   } */
      /* printf ("\n"); */
      process_varw(w_prime, sol, &instance[i], bits, order);
    }
    free(bits);
  }
  /* printf ("END compute_walsh\n"); */
}


// compute walsh coefficients only for hyperplane variables
void compute_walsh_hyperplane(varw_vec *w_prime, Var *sol_reduced, varclause *instance_reduced, int m, Var *sol, int NUMBITS) {
  int i,j,k;
  int order;
  int *bits;                    // store the pattern of walsh terms

  int inCount=0;
  int notCount=0;
  
  /* printf ("BEGIN compute_walsh\n"); */
  /* printf ("m = %d\n", m); */
  for(i = 0; i < m; i++) {      /* for each clause in the instance_reduced */

    /* printf ("i = %d\tnumvars = %d\n", i, instance_reduced[i].numvars); */
    /* printf ("instance_reduced[i].vars\n"); */
    /* for(j = 0; j < instance_reduced[i].numvars; j++) */
    /*   { */
    /*     printf ("%d\t",instance_reduced[i].vars[j]); */
    /*   } */
    /* printf ("\n"); */

    // check if the clause contains any of the hyperplane variables
    if (clause_contain_hyperplane_variable(&(instance_reduced[i]), sol, NUMBITS))
      {
        
        bits = (int*) malloc(sizeof(int) * instance_reduced[i].numvars); // allocate space for all variables in a clause

        for(j = 1; j < pow(2,instance_reduced[i].numvars); j++) { /* for each variable in the clause */
          /* also start from 1 instead of zero, ignore the constant Walsh term (the average over the entire space) */ 
          /* printf ("j = %d\n", j); */
          order = 0;
          for(k = 0; k < instance_reduced[i].numvars; k++) {
            if(CHECK_BIT(j,k)) {    /* only when the related bit is '1', the variable is stored */
              bits[order] = instance_reduced[i].vars[k];
              order++;
              /* printf ("1"); */
            }
          }
          for(k = order; k < instance_reduced[i].numvars; k++) {
            bits[k] = -1;
            /* printf ("0"); */
          }
          /* printf ("\nbits\n"); */
          /* for(k = 0; k < instance_reduced[i].numvars; k++) */
          /*   { */
          /*     printf ("%d\t",bits[k]); */
          /*   } */
          /* printf ("\n"); */
          process_varw(w_prime, sol_reduced, &instance_reduced[i], bits, order);
        }
        free(bits);
        inCount++;
      }
    else
      {
        notCount++;
      }
  }
  // printf("c in = %d, not = %d\n", inCount, notCount);
  /* printf ("END compute_walsh\n"); */
}

// check if the clause c contains any of the bits in sol[0:NUMBITS-1]
bool clause_contain_hyperplane_variable(varclause *c, Var* sol, int NUMBITS)
{
  int i,j, var;
  // bool contain = false;
  
  for (i=0; i<c->numvars; i++)
    {
      var = c->vars[i];
      for (j=0; j<NUMBITS; j++)
        {
          if (var == sol[j].idx)
            {                   // contains
              // printf("c true\n");
              c->w_cofs = (varw_cof**) malloc(sizeof(varw_cof*) * pow(2,c->numvars));
              return true;
            }
        }
    }
  // printf("c false\n");
  return false;
}
