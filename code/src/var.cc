#include <stdio.h>
#include <stdlib.h>
#include "varwalsh.h"
#include "var.h"
#include "varclause.h"

/*--- Helper Functions ---*/
 
//returns 1 if heap[i] < heap[j]
// // inline int mmless(Mediator* m, int i, int j)
// {
//   return (m->data[m->heap[i]] < m->data[m->heap[j]]);
// }
 
// //swaps items i&j in heap, maintains indexes
// int mmexchange(Mediator* m, int i, int j)
// {
//   int t = m->heap[i];
//   m->heap[i]=m->heap[j];
//   m->heap[j]=t;
//   m->pos[m->heap[i]]=i;
//   m->pos[m->heap[j]]=j;
//   return 1;
// }
 
// //swaps items i&j if i<j;  returns true if swapped
// inline int mmCmpExch(Mediator* m, int i, int j)
// {
//   return (mmless(m,i,j) && mmexchange(m,i,j));
// }
 
// //maintains minheap property for all items below i.
// void minSortDown(Mediator* m, int i)
// {
//   for (i*=2; i <= m->minCt; i*=2)
//     {  if (i < m->minCt && mmless(m, i+1, i)) { ++i; }
//       if (!mmCmpExch(m,i,i/2)) { break; }
//     }
// }
 
// //maintains maxheap property for all items below i. (negative indexes)
// void maxSortDown(Mediator* m, int i)
// {
//   for (i*=2; i >= -m->maxCt; i*=2)
//     {  if (i > -m->maxCt && mmless(m, i, i-1)) { --i; }
//       if (!mmCmpExch(m,i/2,i)) { break; }
//     }
// }
 
// //maintains minheap property for all items above i, including median
// //returns true if median changed
// inline int minSortUp(Mediator* m, int i)
// {
//   while (i>0 && mmCmpExch(m,i,i/2)) i/=2;
//   return (i==0);
// }
 
// //maintains maxheap property for all items above i, including median
// //returns true if median changed
// inline int maxSortUp(Mediator* m, int i)
// {
//   while (i<0 && mmCmpExch(m,i/2,i))  i/=2;
//   return (i==0);
// }
 
/*--- Public Interface ---*/
 
 
// //creates new Mediator: to calculate `nItems` running median. 
// //mallocs single block of memory, caller must free.
// Mediator* MediatorNew(int nItems)
// {
//   int size = sizeof(Mediator)+nItems*(sizeof(Item)+sizeof(int)*2);
//   Mediator* m=  (Mediator*)malloc(size);
//   m->data= (Item*)(m+1);
//   m->pos = (int*) (m->data+nItems);
//   m->heap = m->pos+nItems + (nItems/2); //points to middle of storage.
//   m->N=nItems;
//   m->minCt = m->maxCt = m->idx = 0;
//   while (nItems--)  //set up initial heap fill pattern: median,max,min,max,...
//     {  m->pos[nItems]= ((nItems+1)/2) * ((nItems&1)?-1:1);
//       m->heap[m->pos[nItems]]=nItems;
//     }
//   return m;
// }
 
 
// //Inserts item, maintains median in O(lg nItems)
// void MediatorInsert(Mediator* m, Item v)
// {
//   int p = m->pos[m->idx];
//   Item old = m->data[m->idx];
//   m->data[m->idx]=v;
//   m->idx = (m->idx+1) % m->N;
//   if (p>0)         //new item is in minHeap
//     {  if (m->minCt < (m->N-1)/2)  { m->minCt++; }
//       else if (v>old) { minSortDown(m,p); return; }
//       if (minSortUp(m,p) && mmCmpExch(m,0,-1)) { maxSortDown(m,-1); }
//     }
//   else if (p<0)   //new item is in maxheap
//     {  if (m->maxCt < m->N/2) { m->maxCt++; }
//       else if (v<old) { maxSortDown(m,p); return; }
//       if (maxSortUp(m,p) && m->minCt && mmCmpExch(m,1,0)) { minSortDown(m,1); }
//     }
//   else //new item is at median
//     {  if (m->maxCt && maxSortUp(m,-1)) { maxSortDown(m,-1); }
//       if (m->minCt && minSortUp(m, 1)) { minSortDown(m, 1); }
//     }
// }
 
// //returns median item (or average of 2 when item count is even)
// Item MediatorMedian(Mediator* m)
// {
//   Item v= m->data[m->heap[0]];
//   if (m->minCt<m->maxCt) { v=(v+m->data[m->heap[-1]])/2; }
//   return v;
// }
 

/* i: clause id; p: variable id */
int add_clause(var **sol, varclause *instance, int i, int p) {
// #ifdef NDEBUG  
//   printf("c add_clause\n");
// #endif
  (*sol)[p].num_clauses++;      /* number of clauses containing p */
  if(!
     (
      (*sol)[p].clauses = (struct VarClause **) realloc((*sol)[p].clauses, sizeof(varclause*) * (*sol)[p].num_clauses))
     ) {
    /* allocate space the new clause */
    fprintf(stderr,"Failed allocatation of memory for variable %d on %d clause\n",p,(*sol)[p].num_clauses);
    return -1;
  }
  (*sol)[p].clauses[(*sol)[p].num_clauses-1] = &instance[i]; /* associate the new clause variable p */
  return 0;
}

int add_clause_now(var **sol,  int p) {
  // #ifdef NDEBUG
  //   printf("c add_clause_now\n");
  // #endif
  (*sol)[p].num_clauses++;      /* number of clauses containing p */
  return 0;
}

int add_clause_cache(varc **sol, varclausenow *instance, int i, int p) {
// #ifdef NDEBUG  
//   printf("c add_clause_cache\n");
// #endif
  (*sol)[p].num_clauses++;      /* number of clauses containing p */
  if(!
     (
      (*sol)[p].clauses = (struct VarClauseNoW **) realloc((*sol)[p].clauses, sizeof(varclausenow*) * (*sol)[p].num_clauses))
     ) {
    /* allocate space the new clause */
    fprintf(stderr,"Failed allocatation of memory for variable %d on %d clause\n",p,(*sol)[p].num_clauses);
    return -1;
  }
  (*sol)[p].clauses[(*sol)[p].num_clauses-1] = &instance[i]; /* associate the new clause variable p */
  return 0;
}

int add_w_cof(var *sol, int v, int w_id) {
  sol[v].num_w_cofs++;
  if(!(sol[v].w_cofs = (int*) realloc(sol[v].w_cofs, sizeof(int) * sol[v].num_w_cofs))) {
    fprintf(stderr,"Failed allocating memory for %d w_cofs in variable\n",sol[v].num_w_cofs);
    return -1;
  }
  sol[v].w_cofs[sol[v].num_w_cofs-1] = w_id;
  return 0;
}

int build_solution(var **sol, varclause *instance, int n, int m) {
  int i;
  int j;
  *sol = (var*) malloc(sizeof(var) * n);
  
  for(i = 0; i < n; i++) {
    (*sol)[i].idx = i;
    (*sol)[i].value = (drand48() > .5)?1:0; /* initialize solution */
    (*sol)[i].num_clauses = 0;
    (*sol)[i].clauses = NULL;
    (*sol)[i].num_w_cofs = 0;
    (*sol)[i].w_cofs = NULL;
    // (*sol)[i].in_buf = 0;
    // (*sol)[i].last_hp = 0;
    // (*sol)[i].fixed = 0;
    // (*sol)[i].firstorder = 0;
  }
  
  /* scanning the instance */
  for(i = 0; i < m; i++) {
    for(j = 0; j < instance[i].numvars; j++) {
      if(add_clause(sol, instance, i, instance[i].vars[j]) == -1) 
        return -1;
      // (*sol)[instance[i].vars[j]].total_cl = (*sol)[instance[i].vars[j]].total_cl + instance[i].numvars;
      // (*sol)[instance[i].vars[j]].firstorder += (instance[i].signs[j]==1?1:-1);
    }
  }

  // for(i = 0; i < n; i++) {
  //   (*sol)[i].m = MediatorNew((*sol)[i].num_clauses);
  // }
  
  // for(i = 0; i < m; i++) {
  //   for(j = 0; j < instance[i].numvars; j++) {
  //     MediatorInsert((*sol)[instance[i].vars[j]].m,instance[i].numvars);
  //   }
  // }

  return 0;
}

int build_solution_now(var **sol, varclausenow *instance, int n, int m) {
  int i;
  int j;
  *sol = (var*) malloc(sizeof(var) * n);
  
  for(i = 0; i < n; i++) {
    (*sol)[i].idx = i;
    (*sol)[i].value = (drand48() > .5)?1:0; /* initialize solution */
    (*sol)[i].num_clauses = 0;
    (*sol)[i].clauses = NULL;
    (*sol)[i].num_w_cofs = 0;
    (*sol)[i].w_cofs = NULL;
  }
  
  /* scanning the instance to generate sol */
  for(i = 0; i < m; i++) {
    for(j = 0; j < instance[i].numvars; j++) {
      if(add_clause_now(sol, instance[i].vars[j]) == -1) 
        return -1;
    }
  }
  return 0;
}


int build_solution_cache(varc **sol, varclausenow *instance, int n, int m) {
  // cache instance structure for easy retrieval
  int i;
  int j;
  *sol = (varc*) malloc(sizeof(varc) * n);
  
  for(i = 0; i < n; i++) {
    (*sol)[i].idx = i;
    (*sol)[i].value = (drand48() > .5)?1:0; /* initialize solution */
    (*sol)[i].num_clauses = 0;
    (*sol)[i].clauses = NULL;
  }
  
  /* scanning the instance to generate sol */
  for(i = 0; i < m; i++) {
    for(j = 0; j < instance[i].numvars; j++) {
      if(add_clause_cache(sol, instance, i, instance[i].vars[j]) == -1) 
        return -1;
    }
  }
  
  return 0;
}

