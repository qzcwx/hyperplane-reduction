#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "walsh.h"
#include "vectors.h"

int o3_pat[2][4][3] =  { { {0, 0, 0}, {0, 1, 1}, {1, 0, 1}, {1, 1, 0}},
                         { {0, 0, 1}, {0, 1, 0}, {1, 0, 0}, {1, 1, 1}} };
int o2_pat[2][2][2] =  { { {0, 0}, {1, 1} },
                         { {0, 1}, {1, 0} } };
int o1_pat[2][1][1] = { { {0} },
                        { {1} } };

int walsh_sign(var *sol, w_cof *w) {
  int bc = 0;
  int i;

  for(i = 0; i < 3; i++) {
    if(w->vars[i] > -1) {
      if(sol[w->vars[i]].value > 0) {
        bc++;
      }
    }
  }
  
  if(bc % 2 == 0) {
    return 1;
  } else {
    return -1; 
  }
}

int walsh_sign_int(int *sol, w_cof *w) {
  int bc = 0;
  int i;

  for(i = 0; i < 3; i++) {
    if(w->vars[i] > -1) {
      if(sol[w->vars[i]] > 0) {
        bc++;
      }
    }
  }
  if(bc % 2 == 0) {
    return 1;
  } else {
    return -1; 
  }
}


void compute_hyperplanes(clause *instance, int m, int n) {
  int i, j, k;
  int *vars;
  int hp_fitness;

  vars = (int*) malloc(sizeof(int) * n);

  for(i = 0; i < m; i++) {
    for(j = 0; j < 8; j++) {
      hp_fitness = 0;
      if(j == 0) {
        vars[instance[i].vars[0]] = 0;
        vars[instance[i].vars[1]] = 0;
        vars[instance[i].vars[2]] = 0;
      } else if(j == 1) {
        vars[instance[i].vars[0]] = 1;
        vars[instance[i].vars[1]] = 0;
        vars[instance[i].vars[2]] = 0;
      } else if(j == 2) {
        vars[instance[i].vars[0]] = 0;
        vars[instance[i].vars[1]] = 1;
        vars[instance[i].vars[2]] = 0;
      } else if(j == 3) {
        vars[instance[i].vars[0]] = 1;
        vars[instance[i].vars[1]] = 1;
        vars[instance[i].vars[2]] = 0;
      } else if(j == 4) {
        vars[instance[i].vars[0]] = 0;
        vars[instance[i].vars[1]] = 0;
        vars[instance[i].vars[2]] = 1;
      } else if(j == 5) {
        vars[instance[i].vars[0]] = 1;
        vars[instance[i].vars[1]] = 0;
        vars[instance[i].vars[2]] = 1;
      } else if(j == 6) {
        vars[instance[i].vars[0]] = 0;
        vars[instance[i].vars[1]] = 1;
        vars[instance[i].vars[2]] = 1;
      } else if(j == 7) {
        vars[instance[i].vars[0]] = 1;
        vars[instance[i].vars[1]] = 1;
        vars[instance[i].vars[2]] = 1;
      }
      hp_fitness += walsh_sign_int(vars, instance[i].order_3) * instance[i].order_3->value;
      for(k = 0; k < 3; k++) {
        hp_fitness += walsh_sign_int(vars, instance[i].order_2[k]) * instance[i].order_2[k]->value;
        hp_fitness += walsh_sign_int(vars, instance[i].order_1[k]) * instance[i].order_1[k]->value;
      }
      instance[i].hp[j].value[0] = 	vars[instance[i].vars[0]];
      instance[i].hp[j].value[1] = 	vars[instance[i].vars[1]];
      instance[i].hp[j].value[2] = 	vars[instance[i].vars[2]];
      instance[i].hp[j].fitness = hp_fitness;
      instance[i].hp[j].vars[0] = instance[i].vars[0];
      instance[i].hp[j].vars[1] = instance[i].vars[1];
      instance[i].hp[j].vars[2] = instance[i].vars[2];
      if(hp_fitness > 0) {
        instance[i].total_hp_fitness += hp_fitness;
      }
    }
  }
  free(vars);
}

void compute_walsh_signs(var *sol, w_vec *w_prime) {
  int i;

  for(i = 0; i < w_prime->num_w_cofs; i++) {
    w_prime->wb[i].sign = walsh_sign(sol, &w_prime->wb[i]);
  }	
}


int check_w_cof(var *sol, w_vec *w_prime, int *vars) {
  int i, j, found;
  w_cof *check;

  if(sol[vars[0]].num_w_cofs == 0) {
    return -1;
  } else {
    for(i = 0; i < sol[vars[0]].num_w_cofs; i++) {
      check = &w_prime->wb[sol[vars[0]].w_cofs[i]];
      found = 1;
      for(j = 0; j < 3; j++) {
        if(vars[j] != check->vars[j]) {
          found = 0;
        }
      }
      if(found) {
        return check->id;
      }
    }
  }
  return -1;
}


int sum_w_cof(w_cof *w, clause *instance, int *vars) {
  int bc = 0;
  int i, j;

  for(i = 0; i < 3; i++) {
    if(vars[i] > -1) {
      j = 0;
      while(instance->vars[j] != vars[i]) {
        j++;
        if(j >= 3) {
          fprintf(stderr,"Out of bounds error in sum_w_cof\n");

        }
      }
      if(instance->signs[j] < 0) {
        bc++;
      }
    }
  }
  if(bc % 2 == 0) {
    w->value -= 1; 
  } else {
    w->value += 1;
  }

  if(w->order == 1) {
    instance->order_1[instance->order_1_idx++] = w;
  }
  if(w->order == 2) {
    instance->order_2[instance->order_2_idx++] = w;
  }
  if(w->order == 3) {
    instance->order_3 = w; 
    if(w->clause == NULL) {
      w->num_clause = 1;
      w->clause = (clause**) malloc(sizeof(clause*) * 1);
    } else {
      w->num_clause++;
      w->clause = (clause**) realloc(w->clause, sizeof(clause*) * w->num_clause);
    }
    w->clause[w->num_clause-1] = instance;
  } 
  return w->value;
}



int new_w_cof(w_vec *w_prime, clause *instance, var *sol, int *vars) {
  int i;
  int order = 0;

  w_prime->num_w_cofs++;
  w_prime->wb[w_prime->num_w_cofs - 1].value = 0;
  w_prime->wb[w_prime->num_w_cofs - 1].id = w_prime->num_w_cofs - 1;
  w_prime->wb[w_prime->num_w_cofs - 1].num_clause = 0;
  w_prime->wb[w_prime->num_w_cofs - 1].clause = NULL;

  for(i = 0; i < 3; i++) {
    w_prime->wb[w_prime->num_w_cofs - 1].vars[i] = vars[i];
    if(vars[i] > -1) {
      order++;
      if(add_w_cof(sol, vars[i], w_prime->num_w_cofs-1) == -1) {
        fprintf(stderr,"Failed adding w_cof %d to variable %d\n",w_prime->num_w_cofs - 1, vars[i]);
        return -1;
      }
    }
  }

  w_prime->wb[w_prime->num_w_cofs - 1].order = order;
  sum_w_cof(&(w_prime->wb[w_prime->num_w_cofs - 1]), instance, vars);

  return 0;
}


// int evaluate_solution_walsh(w_vec *w_prime, int m) {
//   int i;
//   int eval = 0;

//   for(i = 0; i < w_prime->num_w_cofs; i++) {
//     eval += w_prime->wb[i].value * w_prime->wb[i].sign;
//   }

//   return (eval + 7*m);
// }

int process_vars_restricted(vector_ptr v_ptr, w_vec *w_prime, var *sol, clause *instance, int *vars) {
  int w_id;


  if(v_ptr->restricted[vars[0]] &&
     v_ptr->restricted[vars[1]] &&
     v_ptr->restricted[vars[2]]) {
    return 0;
  }

  if((w_id = check_w_cof(sol, w_prime, vars)) == -1) {
    if(new_w_cof(w_prime, instance, sol, vars) == -1) {
      fprintf(stderr, "Failed adding new w_cof\n");
      return -1;
    } else {
      return 1;
    }
  } else {
    return abs(sum_w_cof(&(w_prime->wb[w_id]), instance, vars));
  }
}




int process_vars(w_vec *w_prime, var *sol, clause *instance, int *vars) {
  int w_id;
  if((w_id = check_w_cof(sol, w_prime, vars)) == -1) {
    if(new_w_cof(w_prime, instance, sol, vars) == -1) {
      fprintf(stderr, "Failed adding new w_cof\n");
      return -1;
    } else {
      return 1;
    }
  } else {
    return abs(sum_w_cof(&(w_prime->wb[w_id]), instance, vars));
  }
}


int build_wb_restricted(w_vec *w_prime, vector_ptr v_ptr, var *sol, clause *instance, int m) {
  int i;
  int vars[3];

  w_prime->wb = (w_cof*) malloc(sizeof(w_cof) * m * 8);
  w_prime->num_w_cofs = 0;

  for(i = 0; i < m; i++) {
    vars[0] = instance[i].vars[0];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars_restricted(v_ptr,w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    vars[0] = instance[i].vars[1];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars_restricted(v_ptr,w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    vars[0] = instance[i].vars[2];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars_restricted(v_ptr,w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 011 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[1];
    vars[2] = -1;
    if(process_vars_restricted(v_ptr,w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    /* 101 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[2];
    vars[2] = -1;
    if(process_vars_restricted(v_ptr,w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 110 */
    vars[0] = instance[i].vars[1];
    vars[1] = instance[i].vars[2];
    vars[2] = -1;
    if(process_vars_restricted(v_ptr,w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 111 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[1];
    vars[2] = instance[i].vars[2];
    if(process_vars_restricted(v_ptr,w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

  }
  return 0;
}

void copy_wb(w_vec *w_prime, w_vec *w_star, int m) {
  int i;

  w_star->wb = (w_cof*) malloc(sizeof(w_cof) * m * 8);
  w_star->num_w_cofs = w_prime->num_w_cofs;

  for(i = 0; i < w_prime->num_w_cofs; i++) {
    w_star->wb[i].id = w_prime->wb[i].id; 
    w_star->wb[i].value = w_prime->wb[i].value;
    w_star->wb[i].order = w_prime->wb[i].order;
    w_star->wb[i].sign = w_prime->wb[i].sign;
    w_star->wb[i].vars[0] = w_prime->wb[i].vars[0];
    w_star->wb[i].vars[1] = w_prime->wb[i].vars[1];
    w_star->wb[i].vars[2] = w_prime->wb[i].vars[2];
    w_star->wb[i].num_clause = w_prime->wb[i].num_clause;
    w_star->wb[i].clause = w_prime->wb[i].clause;
  }

}

int build_wb_var(w_vec *w_prime, var *sol, clause *instance, int m) {
  int i;
  int vars[3];

  w_prime->wb = (w_cof*) malloc(sizeof(w_cof) * m * 8);
  w_prime->num_w_cofs = 0;

  for(i = 0; i < m; i++) {
    vars[0] = instance[i].vars[0];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    vars[0] = instance[i].vars[1];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    vars[0] = instance[i].vars[2];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 011 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[1];
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    /* 101 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[2];
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 110 */
    vars[0] = instance[i].vars[1];
    vars[1] = instance[i].vars[2];
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 111 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[1];
    vars[2] = instance[i].vars[2];
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

  }
  return 0;
}




int build_wb(w_vec *w_prime, var *sol, clause *instance, int m) {
  int i;
  int vars[3];

  w_prime->wb = (w_cof*) malloc(sizeof(w_cof) * m * 8);
  w_prime->num_w_cofs = 0;

  for(i = 0; i < m; i++) {
    vars[0] = instance[i].vars[0];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    vars[0] = instance[i].vars[1];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    vars[0] = instance[i].vars[2];
    vars[1] = -1; 
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 011 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[1];
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }
    /* 101 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[2];
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 110 */
    vars[0] = instance[i].vars[1];
    vars[1] = instance[i].vars[2];
    vars[2] = -1;
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

    /* 111 */
    vars[0] = instance[i].vars[0];
    vars[1] = instance[i].vars[1];
    vars[2] = instance[i].vars[2];
    if(process_vars(w_prime, sol, &instance[i], vars) == -1) {
      fprintf(stderr,"Error processing vars(%d,%d,%d) on clause %d\n",vars[0],vars[1],vars[2],i);
      return -1;
    }

  }
  return 0;
}


void weight_w_prime(w_vec *w_prime, int n) {
  int i;

  for(i = 0; i < w_prime->num_w_cofs; i++) {
    w_cof *w = &w_prime->wb[i]; 
    w->value = (w->order - 2 * n) * w->value;
  }
}








