#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <sys/times.h>
#include <string.h>
#include <limits.h>
#include "varclause.h"
#include "varwalsh.h"
#include "Options.h"
#include <vector>

int parse_instance(char *filename);
int parse_instance_posneg(char *filename);

int evaluate_solution(int* sol, varclausenow* instance, int m){
  // evaluate a solution solution, count the number of satisfied clauses
  int i,j;
  int fit=0;
  for (i=0; i<m; i++){
	int sat=0;
	for (j=0; j<instance[i].numvars; j++){
	  if ((sol[instance[i].vars[j]] * instance[i].signs[j]) > 0){
		sat=1;
		break;
	  }
	}
	fit += sat;
  }
  return fit;
}

void forward_sol(int* sol, int n){
  int i;
  for (i = 0; i<n; i++){
	if (sol[i]<0)				// empty slot
	  {
		sol[i]=1;
		break;
	  }
	else{						
	  sol[i]=-1;
	}
  }
}


void enumerate_solution(varclausenow* instance, int n, int m){
  // print the number of satisfied clauses for all possible assignments
  // -1: false, 1: true
  int i,j;
  int* sol=(int*)malloc(sizeof(int)*n);
  int fit;
  int sum=0;
  for (i = 0; i < n; i++){
	sol[i] = -1;
  }
  
  for (i=0; i<pow(2,n); i++){
	// printf("c %d\n", i);
	fit = evaluate_solution(sol, instance, m);
	sum += fit;

	// print bits and solution
	for (j=0; j<n; j++)
	  {
		printf("c %d", sol[j]>0);
	  }
	printf("c \t%d\n",fit);

	forward_sol(sol,n);

  
  }
  printf("c avg %f\n", sum/pow(2,n));
  free(sol);
}

int parseLine(char* line){
  int i = strlen(line);
  while (*line < '0' || *line > '9') line++;
  line[i-3] = '\0';
  i = atoi(line);
  return i;
}

void shuffle_first_num_bits(struct Var* sol, int n, int num)
{
  /* shuffle the first number bits in sol
     NOTE: what gets shuffled are sol pointers
  */
  int swap_idx;
  struct Var tmp;
  int i;
  
  /* printf ("BEGIN shuffle\n"); */
  /* for (i = 0; i < n; ++i) */
  /*   { */
  /*     printf ("%d\t",sol[i].idx); */
  /*   } */
  /* printf ("\n"); */
  
  /* randomly select a position and swap the value */
  for (i = 0; i < num; ++i)
    {
      swap_idx = rand()%(n-i) + i;
      /* printf ("i=%d, n-i=%d, rand=%d, mod=%d, si=%d\n", i, n-i, tmp, tmp%(n-i), swap_idx); */
      if (i!=swap_idx)
        {                       /* swap the  */
          tmp = sol[i];
          sol[i] = sol[swap_idx];
          sol[swap_idx] = tmp;
        }
      /* else */
      /*   { */
      /*     printf ("remain\n"); */
      /*   } */
    }

  /* for ( j = 0; j < n; ++j) */
  /*   { */
  /*     printf ("%d\t",sol[j].idx); */
  /*   } */
  /* printf ("\n"); */
  /* printf ("END shuffle\n"); */
}

void dynamic_most_frequent_rand(varc* sol, int n, int num){
  // select the current most
  // frequent bit, remove all clause
  // include it and re-select the
  // most frequent bit. The selected
  // 'num' bits are placed at the
  // front of array with
  // randomization in selecting
  // variable with equally top
  // frequencies

  // copy all num_clauses counts to an external array, use this array
  // for finding the most frequent variables
  int * freq_count = (int*) malloc(sizeof(int)*n);
  for (int i = 0; i < n; i++){
	freq_count[i] = sol[i].num_clauses;
	// printf("c %d\n",sol[i].idx);
	// printf("c %d\n",sol[i].num_clauses);
  }

  // for (int l=0; l<n; l++){
  // 	printf("c %d",freq_count[l]);
  // }
  // printf("c \n");


  for (int i = 0; i < num; ++i)
	{
	  std::vector<int> most_freq_var_vec; // variable indices vector
	  std::vector<int> most_freq_count_vec; // variable frequency count vector
	  int most_freq_var = -1;
	  int most_freq_count = -1;

	  varc tmp;
	  int idx = -1;

	  // find the current most frequent variable
	  for (int j = i; j<n; j++){
		idx = sol[j].idx;
		if ( freq_count[idx] > most_freq_count){ // strictly better
		  most_freq_count_vec.clear();
		  most_freq_var_vec.clear();
		}
		if ( freq_count[idx] >= most_freq_count){
		  most_freq_count = freq_count[idx];
		  most_freq_var = j;
		  most_freq_count_vec.push_back(most_freq_count);
		  most_freq_var_vec.push_back(most_freq_var);
		}
	  }
	  
#ifdef NDEBUG  
	  printf("c size = %ld\n", most_freq_var_vec.size());
#endif
	  most_freq_var = most_freq_var_vec[rand() % most_freq_var_vec.size()];
	  
	  // move the most frequent bit to the front by swapping, most_freq_var becomes the ith element
	  if (i != most_freq_var){
		tmp = sol[most_freq_var];
		sol[most_freq_var] = sol[i];
		sol[i] = tmp;
	  }
	  
	  // decay frequency count for variables that co-occur with sol[most_freq_var]
#ifdef NDEBUG  
	  printf("c most_freq=%d\n", sol[i].idx+1);
	  printf("c numclauses=%d\n",sol[i].num_clauses);		  
#endif

	  for (int j = 0; j < sol[i].num_clauses; j++){
// #ifdef NDEBUG
// 		printf("c i=%d, j=%d, num_clauses=%d, clauses=%p\n",i, j, sol[i].num_clauses, sol[i].clauses);
// #endif		
		for (int k = 0; k < (sol[i].clauses[j])->numvars; k++){
// #ifdef NDEBUG
// 		  printf("c %d\t",(sol[i].clauses[j])->vars[k]+1);
// #endif		
		  // for each variable, decrease their num_vars
		  idx = (sol[i].clauses[j])->vars[k];
		  freq_count[idx] -- ;
// #ifdef NDEBUG
// 		  for (int l=0; l<n; l++){
// 		  	printf("c %d",freq_count[l]);
// 		  }
// 		  printf("c \n");
// #endif
		}
	  }
	}
  free(freq_count);
}

void dynamic_most_frequent(varc* sol, int n, int num){
  // select the current most frequent bit, remove all clause include it
  // and re-select the most frequent bit. The selected 'num' bits are
  // placed at the front of array, with bias as in variable indices
  
  // copy all num_clauses counts to an external array, use this array
  // for finding the most frequent variables
  int * freq_count = (int*) malloc(sizeof(int)*n);
  for (int i = 0; i < n; i++){
	freq_count[i] = sol[i].num_clauses;
	// printf("c %d\n",sol[i].idx);
	// printf("c %d\n",sol[i].num_clauses);
  }


#ifdef NDEBUG
	  for (int l=0; l<n; l++){
		printf("c %d",freq_count[l]);
	  }
	  printf("c \n");
#endif


  for (int i = 0; i < num; ++i)
	{
	  int most_freq_var = -1;
	  int most_freq_count = -1;

	  varc tmp;
	  int idx = -1;

	  // find the current most frequent variable
	  for (int j = i; j<n; j++){
		idx = sol[j].idx;
		if ( freq_count[idx] > most_freq_count){
		  most_freq_count = freq_count[idx];
		  most_freq_var = j;
		}
	  }
	  
	  // move the most frequent bit to the front by swapping, most_freq_var becomes the ith element
	  if (i != most_freq_var){
		tmp = sol[most_freq_var];
		sol[most_freq_var] = sol[i];
		sol[i] = tmp;
	  }
	  
	  // decay frequency count for variables that co-occur with sol[most_freq_var]
#ifdef NDEBUG  
	  printf("c most_freq=%d\n", sol[i].idx+1);
	  printf("c numclauses=%d\n",sol[i].num_clauses);		  
#endif

	  for (int j = 0; j < sol[i].num_clauses; j++){
#ifdef NDEBUG
		printf("c i=%d, j=%d, num_clauses=%d, clauses=%p\n",i, j, sol[i].num_clauses, sol[i].clauses);
#endif		
		for (int k = 0; k < (sol[i].clauses[j])->numvars; k++){
#ifdef NDEBUG
		  printf("c %d\t",(sol[i].clauses[j])->vars[k]+1);
#endif		
		  // for each variable, decrease their num_vars
		  idx = (sol[i].clauses[j])->vars[k];
		  freq_count[idx] -- ;
#ifdef NDEBUG
		  for (int l=0; l<n; l++){
		  	printf("c %d",freq_count[l]);
		  }
		  printf("c \n");
#endif
		}
	  }
	}
  free(freq_count);
}


// int getValue(){ //Note: this value is in KB!
//   FILE* file = fopen("/proc/self/status", "r");
//   int result = -1;
//   char line[128];
//   while (fgets(line, 128, file) != NULL){
//     if (strncmp(line, "VmSize:", 7) == 0){
//       result = parseLine(line);
//       break;
//     }
//   }
//   fclose(file);
//   return result;
// }

varclausenow *instance = NULL;
varclause *instance_reduced = NULL; /* REDUCED instance after removing large clauses Instance */
int n = 0; /* Number of atoms */
int m = 0; /* Number of clauses */
int m_reduced = 0; /* Number of clauses */

// variables taking command line options
int seed;
int NUMBITS;
int hyper_set;
int bit_select;
vec<unsigned>* pos; 
vec<unsigned>* neg; 

int int_cmp(const void *a, const void *b) { 
  var *ia = (var *) a;
  var *ib = (var *) b;
  // printf("c ia = %d, ib = %d\n", ia->num_clauses, ib->num_clauses);
  return (ib->num_clauses - ia->num_clauses);
} 

int int_rnd_cmp(const void *a, const void *b) { 
  var *ia = (var *) a;
  var *ib = (var *) b;
  if (ib->num_clauses == ia->num_clauses){
	return drand48()>=0.5 ? 1:-1;
  }
  return (ib->num_clauses - ia->num_clauses);
} 
 
int idx_cmp(const void *a, const void *b) { 
  var *ia = (var *) a;
  var *ib = (var *) b;
	
  return (ib->idx - ia->idx);
} 
 
// int median_cmp(const void *a, const void *b) { 
//   var *ia = (var *) a;
//   var *ib = (var *) b;
//   return ((MediatorMedian(ia->m) - MediatorMedian(ib->m))>0?1:0);
// } 
 
// int len_cmp(const void *a, const void *b) { 
//   var *ia = (var *) a;
//   var *ib = (var *) b;
//   return (abs(ib->firstorder) - abs(ia->firstorder)>0?1:0);
// } 
 
// global command line options
static IntOption  random_seed       ("MAIN", "rnd-seed",    "seed random number generator",  0,  IntRange(0, INT_MAX));
static IntOption  hyperplane_setting ("MAIN", "hyper-set",   "hyperplane setting methods: 0=all zeros, 1=all ones, 2=best hyperplane, 3=random setting, 4=heuristics using positive/negative literals count", 2, IntRange(0, 4));
static IntOption  number_bits ("MAIN", "num-bits",   "number of bits fixed by hyperplane ", 10, IntRange(1, INT_MAX)); 
static IntOption  bit_selection ("MAIN", "bit-select",   "hyperplane bit selection method: 0=random, 1=most frequent, 2=dynamic most frequent", 1, IntRange(0, 2)); 


int main(int argc, char *argv[]) {
  int i, j;
  // int k;
  // int ms[16];
  var *sol = NULL;
  varc *sol_cache = NULL;
  var *sol_reduced = NULL;      // removing long clauses, to get the
                                // reduced sol
  // varclause **instances = NULL; /* Instances */
  // int *vars;
  int *values;
  // char outfile[10000];
  // FILE *outfp;
  char* in;
  int *best_vars=NULL;
  varw_vec w_p;                 /* vector of walsh coefficients */

  
  setUsageHelp("USAGE: %s [options] <input-file>\n\n  where input may be either in plain or gzipped DIMACS.\n");
  parseOptions(argc, argv, true);

  seed = random_seed;
  hyper_set = hyperplane_setting;
  NUMBITS = number_bits;
  bit_select = bit_selection;  
  
  srand48(seed);                
  srand(seed);
#ifdef NDEBUG
  printf ("seed=%d, num_bits=%d, hyper_set=%d, bit_select=%d\n", seed, NUMBITS, hyper_set, bit_select);
#endif
  if (argc >= 2)
    {
      in = argv[1];
    }
  else
    {
      fprintf(stderr,"no input file, exit\n");
      exit(-1);
    }
  
  if (hyper_set==4){			
	parse_instance_posneg(in);
  }else{
	parse_instance(in);
  }
  
   
// #ifdef NDEBUG
//   enumerate_solution(instance, n, m);
// #endif
 
  // vars = (int*) malloc(sizeof(int) * NUMBITS); /* allocate number of vars */
  values = (int*) malloc(sizeof(int) * NUMBITS); /* and their related values */
  
  /*                                      1: number of possible hyperplanes? */
  /*                                      2: 16 is sufficient for numbits=10? *\/ */
  /* vs = malloc(sizeof(vectors) * 16); */
  /* flip_order = malloc(sizeof(int) * n); */

  if (bit_select==2){ 
	if (build_solution_cache(&(sol_cache), instance, n, m)==-1){
	  fprintf(stderr,"error building solution\n");
	  exit(1);
	}	
  }
  else{
	if (build_solution_now(&(sol), instance, n, m)==-1){
	  fprintf(stderr,"error building solution\n");
	  exit(1);
	}	
  }
 
  /* print the built solution */
  /* printf ("build solution\n"); */
  /* for (i = 0; i < n; i++) */
  /*   { */
  /*     printf ("idex %d\tsol %d\n", sol[i].idx, sol[i].value); */
  /*   } */

#ifdef NDEBUG
  printf ("m = %d, m_reduced = %d\n", m, m_reduced);
#endif  
  
  // if(build_solution(&sol_reduced, n) == -1) {
  if(build_solution(&sol_reduced, instance_reduced, n, m_reduced) == -1) {
    fprintf(stderr,"error building solution\n");
    exit(1);
  }
  
  /* printf ("build reduced solution\n"); */
  /* for (i = 0; i < n; i++) */
  /*   { */
  /*     printf ("idex %d\tsol_reduced %d\n", sol_reduced[i].idx, sol_reduced[i].value); */
  /*   } */
  
#ifdef NDEBUG
  printf ("before sort idx+1: \n");
  for (i = 0; i < n; ++i)
    {
	  if (bit_select==2){ 
		printf ("%d\t",sol_cache[i].idx+1);	
	  }
	  else{
		printf ("%d\t",sol[i].idx+1);	
	  }
    }
  printf ("\n");
  printf ("before sort sol_reduced: \n");
  for (i = 0; i < n; ++i)
    {
      printf ("%d\t",sol_reduced[i].idx+1);
    }
  printf ("\n");
#endif
  

  if (bit_select==0){			// random selection
	shuffle_first_num_bits(sol, n, NUMBITS);  /* instead of sorting the entire list, shuffle only the first NUMBITS bits using swap */
  }
  else if (bit_select==1){		// most frequent on raw instance
	qsort(sol, n, sizeof(var), int_cmp); /* sorting variable according to number of associated clause, based on sol, while sol_reduced stay static */
	// qsort(sol, n, sizeof(var), int_rnd_cmp); // randomize ties
  }
  else if (bit_select==2){		// dynamic most frequent  
	// dynamic_most_frequent_rand(sol_cache, n, NUMBITS);
	dynamic_most_frequent(sol_cache, n, NUMBITS);
	
	// copy sol_cache to sol
	sol = (var*) malloc(sizeof(var) * n);
	for(i = 0; i < n; i++) {
	  sol[i].idx = sol_cache[i].idx;
	  sol[i].num_clauses = 0;
	  sol[i].clauses = NULL;
	  sol[i].num_w_cofs = 0;
	  sol[i].w_cofs = NULL;
	}

	// clear sol_cache
	for (i = 0; i<n; i++){
	  free(sol_cache[i].clauses);
	}
	free(sol_cache);
  }
  else{
	printf("c ERROR: wrong bit_selection %d\n", bit_select);
	exit(-1);
  }
  
  /*	qsort(sol,(int) n*.1,sizeof(var),len_cmp); */
  /*	qsort(sol, n*percent, sizeof(var), median_cmp); */
  /*	qsort(sol, numbits, sizeof(var), idx_cmp); */
  
  if (instance){
	for (i = 0; i<m; i++)
	  {
		free(instance[i].vars);
		free(instance[i].signs);
	  }
	free(instance);
  }

  // sorting only change the relative positions of elements in sol
#ifdef NDEBUG
  printf ("after sort idx+1: \n");
  for (i = 0; i < n; ++i)
    {
      printf ("%d\t",sol[i].idx+1);
    }
  printf ("\n");
  printf ("after sort sol_reduced: \n");
  for (i = 0; i < n; ++i)
    {
      printf ("%d\t",sol_reduced[i].idx+1);
    }
  printf ("\n");
#endif

  
  if (hyper_set==2){
    /* walsh tranform is based on "reduced" instance */
    best_vars = (int*) malloc(sizeof(int) * NUMBITS);

    // compute_walsh(&w_p, sol_reduced, instance_reduced, m_reduced);
    // printf("c compute_walsh_hyperplane\n");
    compute_walsh_hyperplane(&w_p, sol_reduced, instance_reduced, m_reduced, sol, NUMBITS);
    // printf("c compute_varhyperplanes_export_random\n");
    compute_varhyperplanes_export_random(&w_p,  n, NUMBITS, sol, sol_reduced, best_vars, argv[1]);
    // printf("c finish export");
#ifdef NDEBUG
    printf("c #w_cofs = %ld\n", w_p.size());
#endif
  }
  
  if (instance_reduced){
	for (i = 0; i<m_reduced; i++)
	  {
		free(instance_reduced[i].vars);
		free(instance_reduced[i].signs);
		if (instance_reduced[i].w_cofs)
		  free(instance_reduced[i].w_cofs);
	  }
	free(instance_reduced);
  }
  // /* store NUMBITS top variables with most variable occurence */
  // for(i = 0; i < NUMBITS; i++) {
  //   vars[i] = sol[i].idx;
  // }
  
// #ifdef NDEBUG  
//   printf ("vars+1\n");
//   for (i = 0; i < NUMBITS; ++i)
//     {
//       printf ("%d\t",sol[i].idx+1);
//     }
//   printf ("\n");
// #endif

  // instances = (varclause**) malloc(sizeof(varclause*) * 16); /* 16 candidate hyperplanes? */
  i=0;                          /* i is never incremented, only the top
                                   hyperplane is considered!! */
  /* store the best assignment suggested by hyperplane average */
  if (hyper_set==0)
    {
      for(j = 0; j < NUMBITS; j++) {
        values[j] = 0;
      }
    }
  else if (hyper_set==1)
    {
      for(j = 0; j < NUMBITS; j++) { 
        values[j] = 1;
      }
    }
  else if (hyper_set==2)		// best hyper
    {
      for(j = 0; j < NUMBITS; j++) { 
        values[j] = best_vars[j];
      }
    }
  else if (hyper_set==3)		// completely random
    {
      for(j = 0; j < NUMBITS; j++) { 
        values[j] = drand48()>=0.5 ? 1:0;
      }
    }
  else if (hyper_set==4){		// pos/neg heuristics for assigning variables
	for(j = 0; j < NUMBITS; j++) { 
	  int idx = sol[j].idx;
	  if((*pos)[idx]>(*neg)[idx]){
		values[j] = 1;
	  }else if ((*pos)[idx]<(*neg)[idx]){
		values[j] = 0;
	  }else{
		values[j] = drand48()>=0.5 ? 1:0;
	  }
	}
	delete pos;
	delete neg;
  }	
  else{
	printf("ERROR: hyper_set %d not found\n", hyper_set);
  }	

  
// #ifdef NDEBUG  
//   printf ("values\n");
//   for ( j = 0; j < NUMBITS; ++j)
//     {
//       printf ("%d",values[j]);
//     }
//   printf ("\n");
// #endif

  // export best hyperplane to file
  char bestfile[10000];
  sprintf(bestfile, "%s.best.%d", argv[1], NUMBITS);
  FILE *bestfp = fopen(bestfile, "w");

  // one line for entire hyperplane
  for (j = 0; j<NUMBITS; j++)
    {
      if (values[j] == 1)
        {
          fprintf(bestfp, "%d\t", (sol[j].idx+1));
        }
      else
        {
          fprintf(bestfp, "%d\t", -(sol[j].idx+1));
        }
    }
  fprintf(bestfp, "0");

  // // one line for each literal
  // for (j = 0; j<NUMBITS; j++)
  //   {
  //     if (values[j] == 1)
  //       {
  //         fprintf(bestfp, "%d\t0\n", (sol[j].idx+1));
  //       }
  //     else
  //       {
  //         fprintf(bestfp, "%d\t0\n", -(sol[j].idx+1));
  //       }
  //   }

  fclose(bestfp);


  // /* compute the hyperplane reduced isntance */
  // ms[i] = reduce_instance(instance, &(instances[i]), sol, vars, values,  m, NUMBITS);
  // // printf ("i=%d, ms[i]=%d\n",i, ms[i]);
  
  // sprintf(outfile,"%s.%d",argv[1],(i+1));
  // /* printf("c name of out file: %s\n", outfile); */
  
  // outfp = fopen(outfile,"w");
  // fprintf(outfp, "p cnf %d %d\n", n, ms[i]+NUMBITS); 
  // fflush(stdout);
  // for(j = 0; j < ms[i]; j++) {
  //   for(k = 0; k < instances[i][j].numvars; k++) {
  //     fprintf(outfp,"%d ",(instances[i][j].vars[k]+1) * instances[i][j].signs[k]);
  //   }
  //   fprintf(outfp,"0\n");
  // }
  // for(j = 0; j < NUMBITS; j++) {
  //   fprintf(outfp,"%d 0\n",(vars[j]+1) * (values[j]?1:-1));
  // }
  // fclose(outfp);
  
  // exit(1);  

  // clearing up allocated memory added by Wenxiang
  // free(vars);
  free(values);
  free(best_vars);

  // for (j = 0; j < ms[i]; j++)
  //   {
  //     free(instances[i][j].vars);
  //     free(instances[i][j].signs);
  //   }
  // free(instances[i]);
  // free(instances);

  if (sol){
	free(sol->w_cofs);
	for(i=0; i<n; i++)
	  {
		if (sol[i].clauses)
		  {
			free(sol[i].clauses);
		  }
	  }
	free(sol);
  }
  
  // free sol_reduced
  if (hyper_set==2)
    {
      for(i=0; i<n; i++)
        {
          if (sol_reduced[i].w_cofs)
            {
              free(sol_reduced[i].w_cofs);
            }
          if (sol_reduced[i].clauses)
            {
              free(sol_reduced[i].clauses);
            }
        }
    }
  free(sol_reduced);
  
  // free w_p
  for (unsigned i=0; i<w_p.size() ; i++)
    {
      free(w_p[i].vars);
    }
  
  return 1;
}


int parse_instance(char *filename) {
  /* parse instance from file */
  FILE *fp;
  int vars[7000], signs[7000], lit, i, idx; /* 7000 is the upper limit for number of variables? */
  char lastc, nextc;
  int reduced_i = 0;
  
  if(!(fp = fopen(filename, "r"))) {
    fprintf(stderr,"Could not open %s\n",filename);
    return -1;
  }

  /* skip commented lines started with 'c' */
  while ((lastc = getc(fp)) == 'c') {
    while ((nextc = getc(fp)) != EOF && nextc != '\n');
  }
  ungetc(lastc,fp);
  if (fscanf(fp,"p cnf %i %i",&n,&m) != 2) {
    fprintf(stderr,"Bad input file\n");
    exit(-1);
  }

  // #ifdef NDEBUG
  //   printf("c sizeof(varclause) = %ld\n", sizeof(varclause));
  // #endif
  
  /* numebr of clauses reduced */
  m_reduced = 0;
  instance = (varclausenow*) malloc(sizeof(varclausenow) * m);         /* for storing instance */
  instance_reduced = (varclause*) malloc(sizeof(varclause) * m);       /* at most m clauses */
  
  for(i = 0;i < m;i++) {
    /* read a complete */
    idx = 0;                    /* idx: length of a clause */
    do {
      if (fscanf(fp,"%i ",&lit) != 1) {
        fprintf(stderr, "Bad input file\n");
        exit(-1);
      }
      if(lit != 0) {
        vars[idx] = abs(lit)-1; /* record variable */
        signs[idx++] = (lit < 0 ? -1 : 1); /* and its related sign */
      }
    }
    while(lit != 0);            /* the end of a clause */
    
    /* store the instance to instance[i] */
    if(idx > 6) {               /* large clause > 6 */
      init_varclause_nowf(&instance[i], idx, vars, signs); // instance is always no walsh coefficients, maybe create a simplier struct for it?
      instance[i].id = i;
    } else {
      init_varclause(&instance_reduced[reduced_i], idx, vars, signs); /* intialize a considered clause */
      instance_reduced[reduced_i].id = reduced_i;
      instance_reduced[reduced_i].orig_numvars = idx;
      reduced_i++;              /* current clause id in reduced instance */
      m_reduced++;              /* maintain number of clauses */
      init_varclause_nowf(&instance[i], idx, vars, signs); 
      instance[i].id = i;
      // max_wcofs += pow(2,idx);  /* upper bound for number of walsh coefficients */
    }
  }
  
  fclose(fp);
  /* printf ("END: parse instance\n"); */

  return 1;
}

int parse_instance_posneg(char *filename) {
  /* parse instance from file */
  FILE *fp;
  int vars[7000], signs[7000], lit, i, idx; /* 7000 is the upper limit for number of variables? */
  char lastc, nextc;
  int reduced_i = 0;
  
  if(!(fp = fopen(filename, "r"))) {
    fprintf(stderr,"Could not open %s\n",filename);
    return -1;
  }

  /* skip commented lines started with 'c' */
  while ((lastc = getc(fp)) == 'c') {
    while ((nextc = getc(fp)) != EOF && nextc != '\n');
  }
  ungetc(lastc,fp);
  if (fscanf(fp,"p cnf %i %i",&n,&m) != 2) {
    fprintf(stderr,"Bad input file\n");
    exit(-1);
  }
  pos = new vec<unsigned>(n,0);
  neg = new vec<unsigned>(n,0);

  // #ifdef NDEBUG
  //   printf("c sizeof(varclause) = %ld\n", sizeof(varclause));
  // #endif
  
  /* numebr of clauses reduced */
  m_reduced = 0;
  instance = (varclausenow*) malloc(sizeof(varclausenow) * m);         /* for storing instance */
  instance_reduced = (varclause*) malloc(sizeof(varclause) * m);       /* at most m clauses */
  
  for(i = 0;i < m;i++) {
    /* read a complete */
    idx = 0;                    /* idx: length of a clause */
    do {
      if (fscanf(fp,"%i ",&lit) != 1) {
        fprintf(stderr, "Bad input file\n");
        exit(-1);
      }
      if(lit != 0) {
        vars[idx] = abs(lit)-1; /* record variable */
		if (lit<0){
		  (*neg)[abs(lit)-1]++;
		  signs[idx]=-1;  
		}else{
		  (*pos)[abs(lit)-1]++;
		  signs[idx]=1;  
		}
		idx++;
      }
    }
    while(lit != 0);            /* the end of a clause */
    
    /* store the instance to instance[i] */
    if(idx > 6) {               /* large clause > 6 */
      init_varclause_nowf(&instance[i], idx, vars, signs); // instance is always no walsh coefficients, maybe create a simplier struct for it?
      instance[i].id = i;
    } else {
      init_varclause(&instance_reduced[reduced_i], idx, vars, signs); /* intialize a considered clause */
      instance_reduced[reduced_i].id = reduced_i;
      instance_reduced[reduced_i].orig_numvars = idx;
      reduced_i++;              /* current clause id in reduced instance */
      m_reduced++;              /* maintain number of clauses */
      init_varclause_nowf(&instance[i], idx, vars, signs); 
      instance[i].id = i;
      // max_wcofs += pow(2,idx);  /* upper bound for number of walsh coefficients */
    }
  }
  
  fclose(fp);
  /* printf ("END: parse instance\n"); */

  return 1;
}
