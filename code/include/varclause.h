#ifndef _VARCLAUSE_H_
#define _VARCLAUSE_H_

/* #include "var.h" */
#include <vector>

#define CHECK_BIT(var,pos) ((var) & (1<<(pos))) /* check the pos^th bit of bitstring var */

/* struct VarW_vec; */
struct VarW_cof;
struct Var;
struct W_cof;


typedef struct VarHyperplane {
  int *vars;
  int *value;
  float fitness;
} varhyperplane;

typedef struct VarClause {
  int id;
  int numvars;
  int orig_numvars;
  int *vars;
  int *signs;
  int numhp;
  int cur_wcof;
  struct VarW_cof **w_cofs;
  varhyperplane *hp;
} varclause;

typedef struct VarClauseNoW {
  int id;
  int numvars;
  int *vars;
  int *signs;
} varclausenow;


void init_varclause_nowf(varclausenow *c, int numvars, int *vars, int *signs);
void init_varclause(varclause *c, int numvars, int *vars, int *signs);
void init_varclause_pos(varclause *c, int numvars, int *vars, int *signs, int* pos);
int reduce_instance(varclause* instance, varclause **newinstance, struct Var *sol, int *var, int *value, int m, int nb);
void compute_varwalsh_signs(struct Var *sol, std::vector<VarW_cof> *w_prime);
void compute_varhyperplanes(std::vector<VarW_cof> *w_prime, int n, int max, struct Var *sol, struct Var *rsol, int *best_vars);
void compute_varhyperplanes_export(std::vector<VarW_cof> *w_prime, int n, int max, struct Var *sol, struct Var *rsol, int *best_vars, char* instance_name);
void compute_varhyperplanes_export_random(std::vector<VarW_cof> *w_prime, int n, int max, struct Var *sol, struct Var *rsol, int *best_vars, char* instance_name);
int is_sat(struct Var *sol, varclause *instance, int i);
void free_varclause(varclause *instance, int m);
void compute_walsh(std::vector<VarW_cof> *w_prime, Var *sol, varclause *instance, int m);
void compute_walsh_hyperplane(std::vector<VarW_cof> *w_prime, Var *sol_reduced, varclause *instance_reduced, int m, Var *sol, int NUMBITS);
bool clause_contain_hyperplane_variable(varclause *c, Var* sol, int NUMBITS);

#endif
