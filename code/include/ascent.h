#ifndef _ASCENT_H_
#define _ASCENT_H_

#include "vectors.h"
#include "clause.h"
#include "var.h"
#include "walsh.h"

typedef int (*greedyPick)(vector_ptr);
typedef clause* (*clausePick)(vector_ptr, clause*, int, var*, w_vec*);
typedef int (*varPick)(vector_ptr, clause*, int, var*, w_vec*);
typedef int (*updateType)(vector_ptr, w_vec*, var*, int);

void local_search(vector_ptr v, clause *i, int m, var *sol, w_vec *w_prime, greedyPick, clausePick, varPick, updateType);
int local_search_v2(vector_ptr v, clause *i, int m, var *sol, w_vec *w_prime, 
		greedyPick gp, clausePick cp, varPick vp, updateType update);

/* GREEDY PICKS */
int s_steepest_greedy_pick(vector_ptr v_ptr);
int s_steepest_greedy_pick_hptabu(vector_ptr v_ptr);
int s_next_greedy_pick(vector_ptr v_ptr);
int s_next_greedy_pick_hp_taboo(vector_ptr v_ptr);
int s_next_greedy_than_avg(vector_ptr v_ptr);
int s_next_greedy_avg_pick_hp_taboo(vector_ptr v_ptr);
int z_next_greedy_pick(vector_ptr v_ptr);
int sa_next_greedy_pick(vector_ptr v_ptr);
int s_next_greedy_pick_sa(vector_ptr v_ptr);

/* CLAUSE PICKS */
clause *walsh_clause_pick(vector_ptr v, clause *instance, int m, var *sol, w_vec *w_prime);
clause *random_clause_pick(vector_ptr v, clause *instance, int m, var *sol, w_vec *w_prime);
clause *unsat_clause_pick(vector_ptr v, clause *instance, int m, var *sol, w_vec *w_prime);

/* VAR PICKS */
int walsh_var_pick(vector_ptr v, clause *instance, int m, var *sol, w_vec *w_prime);
int random_var_pick(vector_ptr v, clause *instance, int m, var *sol, w_vec *w_prime);
int hyperplane_var_pick(vector_ptr v_ptr, clause *c, int m, var *sol, w_vec *w_prime);
int hyperplane_var_pick_tournament(vector_ptr v_ptr, clause *c, int m, var *sol, w_vec *w_prime);
int hyperplane_var_pick_with_noise(vector_ptr v_ptr, clause *c, int m, var *sol, w_vec *w_prime);
int hyperplane_bias_var_pick(vector_ptr v_ptr, clause *c, int m, var *sol, w_vec *w_prime);
int last_var_pick(vector_ptr v_ptr, clause *c, int m, var *sol, w_vec *w_prime);
int novelty_var_pick(vector_ptr v_ptr, clause *c, int m, var *sol, w_vec *w_prime);

#endif
