#ifndef _VAR_H_
#define _VAR_H_

#include "clause.h"
#include "varclause.h"

struct W_vec;
struct VarW_vec;
struct Walsh_list;
struct Vectors;
 
typedef int Item;

/* typedef struct Mediator_t */
/* { */
/*    Item* data;  //circular queue of values */
/*    int*  pos;   //index into `heap` for each value */
/*    int*  heap;  //max/median/min heap holding indexes into `data`. */
/*    int   N;     //allocated size. */
/*    int   idx;   //position in circular queue */
/*    int   minCt; //count of items in min heap */
/*    int   maxCt; //count of items in max heap */
/* } Mediator; */
 

typedef struct Var {            
	int idx;         /* index of variable */
	int value;       /* value setting of variable  */
	int num_clauses; /* number of claused associated with this variable */
	struct VarClause **clauses; /* array of all pointers to the clauses associated with current variable */
	int num_w_cofs;             /* number of w_cofs associated with current variable */
	int *w_cofs;

	/* int in_buf; */
	/* double p[2]; */
	/* int last_hp; */
	/* int fixed; */
	/* int firstorder; */
	/* Mediator *m; */
} var;       /* struct for a single variable in a variable assignment */

typedef struct VarCache {            
  int idx;         /* index of variable */
  int value;       /* value setting of variable  */
  int num_clauses; /* number of claused associated with this variable */
  struct VarClauseNoW **clauses; /* array of all pointers to the clauses associated with current variable */
} varc;       /* struct for a single variable in a variable assignment */

int add_w_cof(var *sol, int v, int w_id);
int build_solution(var **sol, varclause *instance, int n, int m);
int build_solution_now(var **sol, varclausenow *instance, int n, int m);
int build_solution_cache(varc **sol, varclausenow *instance, int n, int m);
int add_clause(var **sol, varclause *instance, int i, int p);
int add_clause_cache(varc **sol, varclausenow *instance, int i, int p);
int add_clause_now(var **sol, int p);


#endif
