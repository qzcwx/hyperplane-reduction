#ifndef _WALSH_H_
#define _WALSH_H_

#include "clause.h"
#include "var.h"


struct Vectors;

typedef struct W_cof {
  int id;
  int value;
  int order;
  int sign;
  int vars[3];
  int num_clause;
  struct Clause **clause;
} w_cof;

typedef struct W_vec {
  int num_w_cofs;
  w_cof *wb;
  double *hyperplane_bias;
} w_vec;

int build_wb(w_vec *w_prime, var *sol, clause *instance, int m);
int build_wb_restricted(w_vec *w_prime, struct Vectors *v_ptr, var *sol, clause *instance, int m);
void compute_walsh_signs(var *sol, w_vec *w_prime);
void compute_hyperplanes(clause *instance, int m, int n);
int evaluate_solution_walsh(w_vec *w_prime, int m);
void weight_w_prime(w_vec *w_prime, int n);

void copy_wb(w_vec *w_prime, w_vec *w_star, int m);

#endif
