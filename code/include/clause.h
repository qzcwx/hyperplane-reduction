#ifndef _CLAUSE_H_
#define _CLAUSE_H_

struct W_cof;

typedef struct Hyperplane {
  int vars[3];
  int value[3];
  int fitness;
} hyperplane;

typedef struct Clause {
  int vars[3];
  int signs[3];
  int in_buf;
  int order_2_idx;
  int order_1_idx;
  int id;
  struct W_cof *order_3;
  struct W_cof *order_2[3];
  struct W_cof *order_1[3];
  /* struct Clause *next; */
  /* struct Clause *prev; */
  hyperplane hp[8];
  int total_hp_fitness;
} clause;

#endif
