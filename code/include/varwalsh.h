#ifndef _VARWALSH_H_
#define _VARWALSH_H_

#include "varclause.h"
#include "var.h"
#include <vector>

typedef struct VarW_cof {       /* individual Walsh terms */
  int id;
  double value;
  int order;
  int sign;
  int *vars;
  int num_clause;
  varclause **clause;
} varw_cof;

/* typedef struct VarW_vec {       /\* vector of Walsh terms, could use  *\/ */
/* 	int num_w_cofs;             /\* current number of walsh terms *\/ */
/* 	varw_cof *wb;                */
/* } varw_vec; */

typedef std::vector<varw_cof> varw_vec;

/* int check_varwcof(var *sol, varw_vec *w_prime, int *vars, int order); */

#endif
