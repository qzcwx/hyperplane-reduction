#ifndef _UTIL_H_
#define _UTIL_H_

#define LIST_CAP 2

#define list_get(a,i,t)     (((t*) (a)->item_list) [(i)])

typedef enum {INT} Type;

/* typedef struct ListItem */
/* { */
/*   void* value; */
/* }list_item; */

typedef struct List
{
  void* item_list;
  int len;
  int cap;
  Type type;
} list;

list* list_init(Type type);
list* list_add_item(list* l, void* data, Type type);
int list_sizeof(Type type);
void list_free(list* l);
void list_clear(list* l);
/* void list_get(list*l, int i, Type type); */


  
#endif
