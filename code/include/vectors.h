#ifndef _S_VEC_
#define _S_VEC_

#include "walsh.h"
#include "clause.h"

typedef struct Vectors *vector_ptr;

typedef struct Clause_buffer {
	clause *head;
	clause *tail;
	int length;
} clause_buffer;

typedef struct Move_buffer {
	var *head;
	var *tail;
	int length;
} move_buffer;

typedef struct Vectors {
	double *s;
	int best_buf;
	int *s_restricted;
	double *z;
	int *w;
	int *sol;
	int numbuffers;
	int *sol_in_buffer;
	int *last_hp_flip;
	int hp_flip_cnt;
	int taboo_limit;
	int *unsat;
	int *whereunsat;
	int numunsat;
	int *oldbuffer;
	int *schanges;
	int *whereschanges;
	int numschanges;
	int *imp;
	int *whereimp;
	int max_clauses;
	int *restricted;
	int numimp;
	int *anumimp;
	int **aimp;
	int *mwhereimp;
	int new_best;
	int random_moves;
	move_buffer s_buffers[7];
	move_buffer z_buffer;
	int s_buffer_size;
	int z_buffer_size;
	int n;
	int m;
	int last_flipped;
	int since_last;
	int iter;
	double eval;
	double cb;
	double noise;
	struct W_vec *ws;
} vectors;

void print_imp(vector_ptr v);
int build_vectors(vector_ptr v, w_vec *w_prime, var *sol, int n, int m);
int build_vectors_withz(vector_ptr v, w_vec *w_prime, var *sol, int n, int m);

/* VECTOR UPDATES */
int update_vectors_buffer(vector_ptr v, w_vec *w_prime, var *sol, int bit);
int update_vectors_clause(vector_ptr v, w_vec *w_prime, var *sol, int bit);
int update_vectors_imp(vector_ptr v, w_vec *w_prime, var *sol, int bit);
int update_vectors_imp_both(vector_ptr v, w_vec *w_prime, var *sol, int bit);
int update_vectors(vector_ptr v, w_vec *w_prime, var *sol, int bit);
int update_vectors_z(vector_ptr v, w_vec *w_prime, var *sol, int bit);
void print_unsat(vector_ptr v);
int free_vectors(vector_ptr v);

#endif
